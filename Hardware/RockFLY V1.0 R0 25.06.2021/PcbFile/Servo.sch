EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L roket-rescue:Servo-servo J2
U 1 1 60984BAF
P 6050 3350
F 0 "J2" H 6280 3396 50  0000 L CNN
F 1 "Servo" H 6280 3305 50  0000 L CNN
F 2 "others:servo" H 6050 3350 50  0001 L BNN
F 3 "" H 6050 3350 50  0001 L BNN
F 4 "Connector" H 6050 3350 50  0001 L BNN "Product_Type"
F 5 "3" H 6050 3350 50  0001 L BNN "Number_of_Positions"
F 6 "2.54 mm[.1 in]" H 6050 3350 50  0001 L BNN "Centerline_Pitch"
F 7 "Compliant" H 6050 3350 50  0001 L BNN "EU_RoHS_Compliance"
F 8 "87527-3" H 6050 3350 50  0001 L BNN "Comment"
	1    6050 3350
	1    0    0    -1  
$EndComp
Text HLabel 5500 3250 0    50   Input ~ 0
Servo
$Comp
L power:+5V #PWR041
U 1 1 60985EDB
P 5150 3300
F 0 "#PWR041" H 5150 3150 50  0001 C CNN
F 1 "+5V" H 5165 3473 50  0000 C CNN
F 2 "" H 5150 3300 50  0001 C CNN
F 3 "" H 5150 3300 50  0001 C CNN
	1    5150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 60986495
P 5500 3500
F 0 "#PWR042" H 5500 3250 50  0001 C CNN
F 1 "GND" H 5505 3327 50  0000 C CNN
F 2 "" H 5500 3500 50  0001 C CNN
F 3 "" H 5500 3500 50  0001 C CNN
	1    5500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3250 5500 3250
Wire Wire Line
	5650 3350 5150 3350
Wire Wire Line
	5150 3350 5150 3300
Wire Wire Line
	5650 3450 5500 3450
Wire Wire Line
	5500 3450 5500 3500
$EndSCHEMATC

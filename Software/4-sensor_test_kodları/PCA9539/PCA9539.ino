#include "PCA9539.h"
#include "Wire.h"

PCA9539 ioport(0x76); // Base address starts at 0x74 for A0 = L and A1 = L

//Base address for PCA9539A devices
//Address   A1    A0
//0x74      L     L
//0x75      L     H
//0x76      H     L
//0x77      H     H
//Be sure to check I2C address with I2C Scanner by Nick Gammon if you are having communication errors
//Some vendors do not hold to this address range. (Diodes Inc, et al)
//http://www.gammon.com.au/i2c


//For pins used as input, the PCA9539A devices do not include pullups.
//You will need to add external pullups if needed.

/**
   @name setup()
   initialize the program
*/
void setup()
{
  Serial.begin(115200);
  Wire.begin(21,22);
  Wire.setClock(400000);
  delay(1000);
  
  ioport.pinMode(pa5, OUTPUT);//pyro1
  ioport.pinMode(pa4, OUTPUT);
  ioport.pinMode(pb3, OUTPUT);//pyro1
  ioport.pinMode(pb4, OUTPUT);//pyro1

  ioport.digitalWrite(pa5, LOW);
  ioport.digitalWrite(pa4, LOW);
  ioport.digitalWrite(pb3, LOW);
  ioport.digitalWrite(pb4, HIGH);
  // delay(1000);
  // // ioport.digitalWrite(pb3, LOW);
  // // ioport.digitalWrite(pb4, LOW);
  // // //ioport.digitalWrite(pa4, LOW);
  // // pinMode(39, INPUT);
  // // //ioport.pinMode(pa5, OUTPUT);//pyro1
  // // //ioport.digitalWrite(pa5, LOW); 
  // // pinMode(36, INPUT);
  // pinMode(12, OUTPUT);
    
  // for(int i=0;i<5;i++){
  //   digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  // delay(100);                       // wait for a second
  // digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  // delay(1000);                       // wait for a second
  // Serial.printf("pyro 1 durum = %f\n",(analogRead(39)*3.3/4096)*6.6);
  // Serial.printf("pyro 2 durum = %f\n",(analogRead(36)*3.3/4096)*6.6);
  // }
    
  //   delay(1000);
  //   ioport.digitalWrite(pb3, HIGH);
  //   ioport.digitalWrite(pa4, HIGH);
  //   delay(100);
  //   ioport.digitalWrite(pb3, LOW);
  //   ioport.digitalWrite(pa4, LOW);
    
  //   delay(1000);
  //   ioport.digitalWrite(pa5, HIGH);
  //   ioport.digitalWrite(pb4, HIGH); 
  //   delay(100);   
  //   ioport.digitalWrite(pb4, LOW); 
  //   ioport.digitalWrite(pa5, LOW); 
}

/**
   @name loop()
   main loop of program and runs endlessly
*/
bool a=0;
void loop()
{ 


}

#include"CC1200.h"				// TI CC1200 RF Radio

//#define MODE // Define this for TX, otherwise code is RX

// Node Addresses
#ifdef MODE
#define THIS_NODE 0x01
#define TARG_NODE 0x02
#else
#define THIS_NODE 0x02
#define TARG_NODE 0x01
#endif

// CC1200 Radio Interrupt Pin
#define RadioTXRXpin	16	// CC1200 Packet Semaphore 

// BUFFER
#define FIFO_SIZE		100		// TX and RX Buffer Size
byte readBytes; // Number of bytes read from RX FIFO
byte rxBuffer[FIFO_SIZE]; // Fill with RX FIFO
byte writeBytes; // Number of bytes written to TX FIFO
byte txBuffer[FIFO_SIZE]; // Fill with TX FIFO

#define idxLength		0		// Length index
#define idxTargetNode	1		// TargetNode index
#define idxFrameCount	3		// FrameCount index
#define TIMEOUT	        10000		// TIMEOUT

union DoubleBytes
{
	double DoubleVal;
	byte AsBytes[sizeof(double)]; // 4 Bytes
} DoubleBytes;

String str = "Hello World!";
char arr[15];


// GLOBAL VARIABLES
byte counter = 0x00;
volatile bool packetSemaphore; // RX/TX success flag. Volatile prevents undesired optimizations by the compiler

// Set Packet Semaphore 
void setSemaphore() {
	packetSemaphore = true;
}

// Clear Packet Semaphore 
void clearSemaphore() {
	packetSemaphore = false;
}


// Try receiving incoming, if any, within the TOUT duration.
bool TryReceive(unsigned long qTimeout) {
	bool receiveStatus = false;
	byte marcstate = 0x00;
	Serial.println("Wait Reception...");

	do
	{
		marcstate = cc1200.GetStat(StatType::MARC_STATE, 0x1F); // MARC_STATE[4:0]
		if (marcstate == MARC_STATE_RX_FIFO_ERR)
		{
			cc1200.FlushRxFifo(); delay(1);
			cc1200.Receive(); delay(3);
			//Serial.println("\tTry RX.");
			Serial.println("\tRXFIFO Flushed!");
		}
		else if (marcstate == MARC_STATE_TX_FIFO_ERR)
		{
			cc1200.FlushTxFifo(); delay(1);
			Serial.println("\tTXFIFO Flushed!");
		}
		else if (marcstate >= MARC_STATE_XOFF && marcstate <= MARC_STATE_ENDCAL)
		{
			Serial.print("\tSettling: "); Serial.println(marcstate);
		}
		else if (marcstate != MARC_STATE_RX)
		{
			//Serial.println("\tTry RX.");
			cc1200.Receive(); delay(7);
		}

		// Timeout Implementation
		if (millis() >= qTimeout) //isTimeOut(qTimeout)
		{
			Serial.println("\tTOUT!");
			cc1200.Idle();
			break;
		}

		delay(10);
	} while (!packetSemaphore);

	// Packet received i.e. no timeout occurred
	if (packetSemaphore)
	{
		readBytes = cc1200.ReadRxFifo(rxBuffer);

		if (readBytes >= 0) // Fail-Safe. 2 bytes header (Len-Adrs) + 2 appended bytes (CRC-RSSI) footer
		{
			receiveStatus = true;
		}

		cc1200.Idle(); delay(1);
		cc1200.FlushRxFifo();
		clearSemaphore();
	}

	return receiveStatus;
}
void setup(){
    // SERIAL
	Serial.begin(115200);
	Serial.println("\n>>Start Setup Chain");

	// CC1200 RADIO
	cc1200.Init(SS, MOSI, MISO, SCK, PIN_UNUSED); // SS, MOSI, MISO, SCK, RadioResetpin
	cc1200.Configure(rxSniffSettings, rxSniffSettLen); // 2sec internal delay
	cc1200.SetAddress(THIS_NODE); delay(10);
	cc1200.Strobe(CC120X_SCAL); delay(1000);
	byte readNode = cc1200.GetAddress(false);
	Serial.print("\tNode: "); Serial.println(readNode);
	if (THIS_NODE == readNode)
	{
        Serial.print("\tNode "); Serial.print(THIS_NODE); Serial.println(" Ok!");
	}
    else{
        Serial.print("\tERROR \n");
        while (true);
    }
	cc1200.FlushRxFifo(); delay(100);
	cc1200.FlushTxFifo(); delay(100);
	Serial.println("\tRadio Config");

	// RADIO INTERRUPT
	packetSemaphore = false;
	pinMode(RadioTXRXpin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(RadioTXRXpin), setSemaphore, FALLING); // LOW, CHANGE, RISING, FALLING
	Serial.println("\tPacket Interrupt Config");

    Serial.flush();

}
float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
byte SIV;
long unix_time;
int ii=0;
int c=0;
String All_data1;
void loop(){
if (TryReceive(TIMEOUT + millis()))
{
    Serial.print(F("RX: "));
    for (int i = 0; i < readBytes; i++)
    {
        Serial.print(rxBuffer[i]); Serial.print(F(", "));
    }
    Serial.println(F(""));
    // DoubleBytes.AsBytes[0] = rxBuffer[4];
    // DoubleBytes.AsBytes[1] = rxBuffer[5];
    // DoubleBytes.AsBytes[2] = rxBuffer[6];
    // DoubleBytes.AsBytes[3] = rxBuffer[7];
    // Serial.print(F("Double: ")); Serial.println(DoubleBytes.DoubleVal);
    Serial.print(F("\tText: "));
    c=4;
for( ii=0;ii<3;ii++){ 
H3LIS331_data[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}
for( ii=0;ii<2;ii++){ 
Ms5611_data[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}

for( ii=0;ii<3;ii++){ 
bnoAccel[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bnoGyro[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}

for( ii=0;ii<3;ii++){ 
bnoMag[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bnoRaw[ii]= ByteToFloat(rxBuffer,c);
c+=4;
}

for( ii=0;ii<3;ii++){ 
GPS_data[ii]= ByteTolong(rxBuffer,c);
c+=4;
}

SIV=rxBuffer[c];
c+=1;

unix_time= ByteTolong(rxBuffer,c);

c=0;

All_data1=String(H3LIS331_data[0])+","+String(H3LIS331_data[1])+","+String(H3LIS331_data[2])+",";
    All_data1+=String(Ms5611_data[0])+","+String(Ms5611_data[1])+",";
    All_data1+=String(bnoAccel[0])+","+String(bnoAccel[1])+","+String(bnoAccel[2])+",";
    All_data1+=String(bnoGyro[0])+","+String(bnoGyro[1])+","+String(bnoGyro[2])+",";
    All_data1+=String(bnoMag[0])+","+String(bnoMag[1])+","+String(bnoMag[2])+",";
    All_data1+=String(bnoRaw[0])+","+String(bnoRaw[1])+","+String(bnoRaw[2])+",";
    All_data1+=String(GPS_data[0])+","+String(GPS_data[1])+","+String(GPS_data[2])+",";
    All_data1+=String(SIV)+","+String(unix_time)+"\n";
   // All_data1+=String(GPS_time[0])+"-"+String(GPS_time[1])+"-"+String(GPS_time[2])+",";
   // All_data1+=String(GPS_time[3])+"."+String(GPS_time[4])+"."+String(GPS_time[5])+"\n";
 Serial.print(All_data1);
}

}
float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }
/**************************************************************************
 This is an example for our Monochrome OLEDs based on SSD1306 drivers

 Pick one up today in the adafruit shop!
 ------> http://www.adafruit.com/category/63_98

 This example is for a 128x64 pixel display using I2C to communicate
 3 pins are required to interface (two I2C and one reset).

 Adafruit invests time and resources providing this open
 source code, please support Adafruit and open-source
 hardware by purchasing products from Adafruit!

 Written by Limor Fried/Ladyada for Adafruit Industries,
 with contributions from the open source community.
 BSD license, check license.txt for more information
 All text above, and the splash screen below must be
 included in any redistribution.
 **************************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library. 
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
//#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire,-1);

#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16

int pushButton = 0;
bool screenMode=1;
uint8_t switchScreen=0;
void setup() {
  Serial.begin(9600);

  pinMode(pushButton, INPUT_PULLUP);
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    screenMode=0;
  }
  display.clearDisplay();
  

}

void loop() {
  printData();    // Draw 'stylized' characters
}

void printData(void) {
  if(screenMode){
  if(!digitalRead(pushButton)){
  //while(!digitalRead(pushButton));
  switchScreen++;
  if(switchScreen>4)switchScreen=0;
  }		

  if(switchScreen==0) printGPS();
  else if(switchScreen==1) printMs5611();
  else if(switchScreen==2) printBno1();
  else if(switchScreen==3) printBno2();
  else if(switchScreen==4) print400g();
  delay(90);
  }
}

void printGPS(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.print(F("GPS Fix  : "));display.println(F("Ok"));
  display.print(F("GPS Time : "));display.println(F("15:06"));
  display.print(F("GPS Date : "));display.println(F("16.02.2022"));
  display.print(F("GPS Vel  : "));display.print(F("0.45"));display.println(F(" m/s"));
  display.print(F("GPS Lat  : "));display.print(F("37.050809"));display.write(9);display.println(F(""));
  display.print(F("GPS Long : "));display.print(F("28.366684"));display.write(9);display.println(F(""));
  display.print(F("GPS Alt  : "));display.println(F("14300"));
  display.print(F("GPS Sat  : "));display.println(F("10"));
  display.display();
}
void printMs5611(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.print(F("Temp     : "));display.print(F("26.22 "));display.write(9);display.println(F("C"));
  display.print(F("Altitude : "));display.print(F("2.45"));display.println(F(" m"));
  display.print(F("Velocity : "));display.print(F("0.15"));display.println(F(" m/s"));
  display.print(F("Battery  : "));display.print(F("3.85"));display.println(F(" V"));
  display.print(F("Main Pyro: "));display.println(F("Ok"));
  display.print(F("Drog Pyro: "));display.println(F("Ok"));
  display.print(F("Drogue Meter: "));display.print(F("50"));display.println(F(" m"));
  display.display();
}
void printBno2(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.print(F("Gyro-X   : "));display.print(F("0.22"));display.println(F(" rad/s"));
  display.print(F("Gyro-Y   : "));display.print(F("0.22"));display.println(F(" rad/s"));
  display.print(F("Gyro-Z   : "));display.print(F("0.22"));display.println(F(" rad/s"));
  display.println(F(""));
  display.print(F("Magno-X  : "));display.print(F("79.45"));display.println(F(" uT"));
  display.print(F("Magno-Y  : "));display.print(F("39.05"));display.println(F(" uT"));
  display.print(F("Magno-Z  : "));display.print(F("11.36"));display.println(F(" uT"));
  display.display();
}
void printBno1(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.print(F("Accel-X  : "));display.print(F("0.22"));display.println(F(" g"));
  display.print(F("Accel-Y  : "));display.print(F("0.45"));display.println(F(" g"));
  display.print(F("Accel-Z  : "));display.print(F("1.15"));display.println(F(" g"));
  display.println(F(""));
  display.print(F("Yaw-X    : "));display.print(F("160.45 "));display.write(9);display.println(F(""));
  display.print(F("Pitch-Y  : "));display.print(F("137.05 "));display.write(9);display.println(F(""));
  display.print(F("Roll-Z   : "));display.print(F("28.36 "));display.write(9);display.println(F(""));
  display.display();
}
void print400g(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  display.print(F("400g Acc-X : "));display.print(F("0.22"));display.println(F(" g"));
  display.print(F("400g Acc-Y : "));display.print(F("0.45"));display.println(F(" g"));
  display.print(F("400g Acc-Z : "));display.print(F("1.15"));display.println(F(" g"));
  display.display();
}
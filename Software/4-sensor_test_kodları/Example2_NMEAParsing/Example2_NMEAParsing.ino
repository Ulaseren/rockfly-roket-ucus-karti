/*
  Read NMEA sentences over I2C using Ublox module SAM-M8Q, NEO-M8P, etc
  By: Nathan Seidle
  SparkFun Electronics
  Date: August 22nd, 2018
  License: MIT. See license file for more information but you can
  basically do whatever you want with this code.

  This example reads the NMEA characters over I2C and pipes them to MicroNMEA
  This example will output your current long/lat and satellites in view
 
  Feel like supporting open source hardware?
  Buy a board from SparkFun!
  ZED-F9P RTK2: https://www.sparkfun.com/products/15136
  NEO-M8P RTK: https://www.sparkfun.com/products/15005
  SAM-M8Q: https://www.sparkfun.com/products/15106

  For more MicroNMEA info see https://github.com/stevemarple/MicroNMEA

  Hardware Connections:
  Plug a Qwiic cable into the GPS and a BlackBoard
  If you don't have a platform with a Qwiic connection use the SparkFun Qwiic Breadboard Jumper (https://www.sparkfun.com/products/14425)
  Open the serial monitor at 115200 baud to see the output
  Go outside! Wait ~25 seconds and you should see your lat/long
*/

#include <Wire.h> //Needed for I2C to GPS

#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
SFE_UBLOX_GPS myGPS;

#include <MicroNMEA.h> //http://librarymanager/All#MicroNMEA
char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));

void setup()
{
  Serial.begin(115200);
  Serial.println("SparkFun Ublox Example");

  Wire.begin();
  Wire.setClock(400000);
  if (myGPS.begin() == false)
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }
  //myGPS.setUART1Output(0); //Disable the UART1 port output 
  //myGPS.setUART2Output(0); //Disable Set the UART2 port output
  //myGPS.setI2COutput(COM_TYPE_NMEA); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.setNavigationFrequency(10);
  //myGPS.saveConfiguration();        //Save the current settings to flash and BBR
}
long GPS_data[3];
int GPS_time[6];
uint8_t SIV;
unsigned long start=0;
unsigned long stop=0;
void loop()
{
  start=millis();
  myGPS.checkUblox(); //See if new data is available. Process bytes as they come in.

  if(nmea.isValid() == true)
  {
    
    GPS_data[0]  = nmea.getLatitude();
    GPS_data[1]  = nmea.getLongitude();
    nmea.getAltitude(GPS_data[2]);
    SIV=nmea.getNumSatellites();
    GPS_time[0]=nmea.getYear();
    GPS_time[1]=nmea.getMonth();
    GPS_time[2]=nmea.getDay();
    
    GPS_time[3]=nmea.getHour();
    GPS_time[4]=nmea.getMinute();
    GPS_time[5]=nmea.getSecond();
    stop=millis();
    Serial.println(stop-start);
    Serial.printf("Lat: %ld Long: %ld (degrees * 10^-7) Alt: %ld (mm) SIV: %d \n",GPS_data[0],GPS_data[1],GPS_data[2],SIV);
    Serial.printf("%d-%02d-%02d %02d:%02d:%02d\n",GPS_time[0],GPS_time[1],GPS_time[2],GPS_time[3],GPS_time[4],GPS_time[5]);
  }
  else
  {
    Serial.print("No Fix - ");
    Serial.print("Num. satellites: "); 
    Serial.println(nmea.getNumSatellites());
    stop=millis();
    Serial.println(stop-start);
  }

  delay(100); //Don't pound too hard on the I2C bus
}

//This function gets called from the SparkFun Ublox Arduino Library
//As each NMEA character comes in you can specify what to do with it
//Useful for passing to other libraries like tinyGPS, MicroNMEA, or even
//a buffer, radio, etc.
void SFE_UBLOX_GPS::processNMEA(char incoming)
{
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

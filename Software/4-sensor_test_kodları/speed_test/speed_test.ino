// Basic demo for readings from Adafruit BNO08x
#include <Adafruit_BNO08x.h>

// For SPI mode, we need a CS pin
#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14

Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

uint8_t MS_CS = 2;
uint8_t accel200gCSPin = 17;
void setup(void) {
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);

  Serial.begin(115200);
  while (!Serial)
    delay(10);  // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER)) {
    Serial.println("Could not enable accelerometer");
  }
}

float x,y,z;
int stime=0;
int oldTime=0;
float speed=0;
float oldSpeed=0;
float filteredSpeed=0;

void loop() {
  delay(10);

  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {
    case SH2_ACCELEROMETER:
      stime=millis();
      x=sensorValue.un.accelerometer.x;
      y=sensorValue.un.accelerometer.y;
      z=sensorValue.un.accelerometer.z;
      speed =oldSpeed+(sqrt(x*x+y*y+z*z)-9.80665)*(stime-oldTime)/1000; 
      oldTime=stime;
      oldSpeed=speed;
      Serial.printf("speed = %f\n",filteredSpeed); 
      break;
  }
}

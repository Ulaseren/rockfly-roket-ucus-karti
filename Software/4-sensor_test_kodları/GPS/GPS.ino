#include <Wire.h>
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS

SFE_UBLOX_GPS myGPS;
long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.

long GPS_data[3];
int GPS_time[6];
byte SIV;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  myGPS.begin();
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.setNavigationFrequency(1);
  myGPS.saveConfiguration();
}

void loop() {
  if ((millis() - lastTime > 1000))
  {

    lastTime = millis(); //Update the timer
    GPS_data[0] = myGPS.getLatitude();
    GPS_data[1] = myGPS.getLongitude();
    GPS_data[2] = myGPS.getAltitude();
    SIV = myGPS.getSIV();
    GPS_time[0]=myGPS.getYear();
    GPS_time[1]=myGPS.getMonth();
    GPS_time[2]=myGPS.getDay();
    GPS_time[3]=myGPS.getHour();
    GPS_time[4]=myGPS.getMinute();
    GPS_time[5]=myGPS.getSecond();
    
    Serial.printf("Lat: %ld Long: %ld (degrees * 10^-7) Alt: %ld (mm) SIV: %02X \n",GPS_data[0],GPS_data[1],GPS_data[2],(int)SIV);
    Serial.printf("%d-%02d-%02d %02d:%02d:%02d\n",GPS_time[0],GPS_time[1],GPS_time[2],GPS_time[3],GPS_time[4],GPS_time[5]);
  
  }
}

/*
  Using the BNO080 IMU
  By: Nathan Seidle
  SparkFun Electronics
  Date: July 27th, 2018
  SparkFun code, firmware, and software is released under the MIT License.
	Please see LICENSE.md for further details.

  Feel like supporting our work? Buy a board from SparkFun!
  https://www.sparkfun.com/products/14686

  This example shows how to use the SPI interface and print two records at the same time:
  Accel and Quat.

  This example shows how to output the i/j/k/real parts of the rotation vector.
  https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation

  Hardware modifications:
  The PS1 jumper must be closed
  The PS0 jumper must be open. PS0/WAKE is connected and the WAK pin is used to bring the IC out of sleep.
  The I2C pull up jumper must be cleared/open

  Hardware Connections:
  Don't hook the BNO080 to a normal 5V Uno! Either use the Qwiic system or use a
  microcontroller that runs at 3.3V.
  Arduino 13 = BNO080 SCK
  12 = SO
  11 = SI
  10 = !CS
  9 = WAK
  8 = !INT
  7 = !RST
  3.3V = 3V3
  GND = GND
*/

#include <SPI.h>

#include "SparkFun_BNO080_Arduino_Library.h"  // Click here to get the library: http://librarymanager/All#SparkFun_BNO080
BNO080 myIMU;

//These pins can be any GPIO
int imuCSPin = 5;
int imuWAKPin = -1;
int imuINTPin = 15;
int imuRSTPin = 2;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println("BNO080 SPI Read Example");

  // myIMU.enableDebugging(Serial); //Pipe debug messages to Serial port

  if (myIMU.beginSPI(imuCSPin, imuWAKPin, imuINTPin, imuRSTPin) == false) {
    Serial.println("BNO080 over SPI not detected. Are you sure you have all 6 connections? Freezing...");
   // while (1);
  }

  //You can also call begin with SPI clock speed and SPI port hardware
  //myIMU.beginSPI(imuCSPin, imuWAKPin, imuINTPin, imuRSTPin, 1000000);
  //myIMU.beginSPI(imuCSPin, imuWAKPin, imuINTPin, imuRSTPin, 1000000, SPI1);

  //The IMU is now connected over SPI
  //Please see the other examples for library functions that you can call


  myIMU.enableAccelerometer(20);  //Send data update every 50ms
  myIMU.enableGyro(20);           //Send data update every 50ms
  myIMU.enableMagnetometer(20);   //Send data update every 50ms
  myIMU.enableRotationVector(20);

  Serial.println(F("Rotation vector enabled"));
  Serial.println(F("Output in form i, j, k, real, accuracy"));
}
float x_Accel, y_Accel, z_Accel;
float x_Gyro, y_Gyro, z_Gyro;
float x_Mag, y_Mag, z_Mag;
float Roll, Pitch, Yaw;
String All_data;
unsigned long start = 0;
unsigned long end = 0;
void loop() {
  //Look for reports from the IMU
  myIMU.dataAvailable();
  if(millis()-start>20){
    start = millis();
    x_Accel = myIMU.getAccelX();
    y_Accel = myIMU.getAccelY();
    z_Accel = myIMU.getAccelZ();
    x_Gyro = myIMU.getGyroX();
    y_Gyro = myIMU.getGyroY();
    z_Gyro = myIMU.getGyroZ();
    x_Mag = myIMU.getMagX();
    y_Mag = myIMU.getMagY();
    z_Mag = myIMU.getMagZ();
    Roll = (myIMU.getRoll()) * 180.0 / PI;
    Pitch = (myIMU.getPitch()) * 180.0 / PI;
    Yaw = (myIMU.getYaw()) * 180.0 / PI;
    All_data = String(x_Accel) + "," + String(y_Accel) + "," + String(z_Accel) + ",";
    All_data += String(x_Gyro) + "," + String(y_Gyro) + "," + String(z_Gyro) + ",";
    All_data += String(x_Mag) + "," + String(y_Mag) + "," + String(z_Mag) + ",";
    All_data += String(Roll) + "," + String(Pitch) + "," + String(Yaw) + "\n";
    Serial.println(All_data);
  }
  if(millis()-end>50){
    end = millis();
  }
  
}

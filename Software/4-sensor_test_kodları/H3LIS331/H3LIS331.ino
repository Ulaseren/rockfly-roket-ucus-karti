#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>

Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

sensors_event_t event;
float H3LIS331_data[3];
long lastTime = 0;

void setup() {
  Serial.begin(115200);
  Wire.begin();

  lis.begin_I2C();   // change this to 0x19 for alternative i2c address
  lis.setRange(H3LIS331_RANGE_100_G);   // 100, 200, or 400 G!
  /*
  Serial.print("Range set to: ");
  switch (lis.getRange()) {
    case H3LIS331_RANGE_100_G: Serial.println("100 g"); break;
    case H3LIS331_RANGE_200_G: Serial.println("200 g"); break;
    case H3LIS331_RANGE_400_G: Serial.println("400 g"); break;
  }
  */
 lis.setDataRate(LIS331_DATARATE_50_HZ);
 /*
  Serial.print("Data rate set to: ");
  switch (lis.getDataRate()) {
    case LIS331_DATARATE_POWERDOWN: Serial.println("Powered Down"); break;
    case LIS331_DATARATE_50_HZ: Serial.println("50 Hz"); break;
    case LIS331_DATARATE_100_HZ: Serial.println("100 Hz"); break;
    case LIS331_DATARATE_400_HZ: Serial.println("400 Hz"); break;
    case LIS331_DATARATE_1000_HZ: Serial.println("1000 Hz"); break;
    case LIS331_DATARATE_LOWPOWER_0_5_HZ: Serial.println("0.5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_1_HZ: Serial.println("1 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_2_HZ: Serial.println("2 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_5_HZ: Serial.println("5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_10_HZ: Serial.println("10 Hz Low Power"); break;
  }
*/  
}

void loop() {

  if ((millis() - lastTime > 20))
  {

  lastTime = millis(); //Update the timer
  lis.getEvent(&event);

  H3LIS331_data[0]=event.acceleration.x/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1]=event.acceleration.y/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2]=event.acceleration.z/SENSORS_GRAVITY_STANDARD;
  
  Serial.printf("X: %f,Y: %f,Z: %f g\n",H3LIS331_data[0], H3LIS331_data[1], H3LIS331_data[2]);

  }

}

#include <Wire.h>
#include "SparkFun_BNO080_Arduino_Library.h"

BNO080 myIMU;

float x_Accel,y_Accel,z_Accel;
float x_Gyro,y_Gyro,z_Gyro;
float x_Mag,y_Mag,z_Mag;
float Roll,Pitch,Yaw;

long lastTime=0;
void setup() {
  Serial.begin(115200);

  Wire.begin();
  Wire.setClock(400000);

  myIMU.begin();
  myIMU.enableAccelerometer(50); //Send data update every 50ms
  myIMU.enableGyro(50); //Send data update every 50ms
  myIMU.enableMagnetometer(50); //Send data update every 50ms
  myIMU.enableRotationVector(50);

}

void loop() {
  if ((millis() - lastTime > 50))
  {
    lastTime=millis();
    myIMU.dataAvailable();
    delay(1);
    x_Accel = myIMU.getAccelX();
    y_Accel = myIMU.getAccelY();
    z_Accel = myIMU.getAccelZ();
    x_Gyro = myIMU.getGyroX();
    y_Gyro = myIMU.getGyroY();
    z_Gyro = myIMU.getGyroZ();
    x_Mag = myIMU.getMagX();
    y_Mag = myIMU.getMagY();
    z_Mag = myIMU.getMagZ();
    Roll =(myIMU.getRoll()) * 180.0 / PI;
    Pitch = (myIMU.getPitch()) * 180.0 / PI;
    Yaw = (myIMU.getYaw()) * 180.0 / PI;

    Serial.printf("Accel- X: %f Y: %f Z: %f\n",x_Accel,y_Accel,z_Accel);
    Serial.printf("Gyro- X: %f Y: %f Z: %f\n",x_Gyro,y_Gyro,z_Gyro);
    Serial.printf("Mag- X: %f Y: %f Z: %f\n",x_Mag,y_Mag,z_Mag);
    Serial.printf("Rotation- Roll: %f Pitch: %f Yaw: %f\n",Roll,Pitch,Yaw);

  }
}

#include "MS5611.h"

MS5611 MS5611(0x76);   // 0x76 = CSB to VCC; 0x77 = CSB to GND

long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.
float Ms5611_data[2];
void setup() {
  Serial.begin(115200);
  Wire.begin(21,22);
  MS5611.begin();

}

void loop() {
  if ((millis() - lastTime > 20))
  {

  lastTime = millis(); //Update the timer
  MS5611.read();
  Ms5611_data[0]=MS5611.getTemperature();
  Ms5611_data[1]=MS5611.getPressure();
  
  Serial.printf("T:%.2f,P:%.2f\n",Ms5611_data[0],Ms5611_data[1]);

  }
}

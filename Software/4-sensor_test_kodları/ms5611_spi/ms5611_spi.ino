#include "ms5611_spi.h"
#include <RadioLib.h>
#include "SPI.h"
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO08x.h>
// CHANGE THIS TO WHATEVER PINS YOU'RE USING TO CONNECT TO THE MS5611.
// Arg0: pin that is connected to ms5611 pins 4/5
// Arg1: pin that is connected to ms5611 pin 6
// Arg2: pin that is connected to ms5611 pin 7
// Arg3: pin that is connected to ms5611 pin 8
// Arg4: Time, in microseconds, to delay between rising and falling clock cycles. 2usec should be fine.
SX1262 radio = new Module(15, 4, 32, 33);
uint8_t MS_CS = 2;
#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;
uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
baro_ms5611 MS5611(MS_CS);

void setup() {
  pinMode(BNO08X_CS, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1); 
  Serial.begin(115200);
  delay(100);
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }  
  delay(100);
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");
  Serial.println("Reading events");
  delay(100);
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
  MS5611.initialize(); 
  MS5611.calibrateAltitude();
}
float altitude;
float speed;
unsigned long start=0;
unsigned long end=0;
void loop() {
  delay(10);
  start=micros();
  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  //Serial.print(MS5611.getTemperature_degC());
  //Serial.print("C ");
  //Serial.print(MS5611.getPressure_mbar());
  //Serial.print("mbar ");

  end=micros();
  //Serial.print(MS5611.getPressure_mbar());
  //Serial.print(MS5611.getCalAltitude());
  //Serial.print("\t");
  Serial.println( MS5611.getAltitudeKalman());
  //Serial.print("\t");
  //Serial.println( MS5611.getVelFps());
  //Serial.print("meter ");
  //Serial.println(end-start);
  //Serial.println("meter(kalman) ");
  //Serial.print(MS5611.getVelFps());
  //Serial.println("fps ");
  
  //Serial.printf("takildi suresi 400G: %d \n",end2-start2);
  
}

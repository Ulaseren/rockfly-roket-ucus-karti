#include "FS.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_BNO08x.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "LITTLEFS.h"
#include <MicroNMEA.h>
#include "ms5611_spi.h"
#include "PCA9539.h"
#include <RadioLib.h>
#include "SparkFun_Ublox_Arduino_Library.h"  
//#include "TimeLib.h"
#include <Vector.h>
#include <WiFi.h>              // Built-in
#include <WebServer.h>
#include <ESP32Time.h>
#include "EEPROM.h"
#include <SimpleKalmanFilter.h>

uint8_t timeZone = 3;
float mainPar=50;  //main paraşüt değiştirme

bool flightMode = 0;
bool writeMode = 0;

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire,-1);
bool screenMode=1;
uint8_t switchScreen=0;
uint8_t printDataCount =0;

SimpleKalmanFilter speedKalmanFilter(1, 1, 0.1);
SimpleKalmanFilter accelKalmanFilter(1, 1, 0.1);
ESP32Time rtc;

#define ServerVersion "1.0"
String  webpage = "";
#define SPIFFS LITTLEFS
bool    SPIFFS_present = false;
const char* ssid = "RockFLY2022";
const char* password = "rockfly2022";

// Variables to save values from HTML form
String apooge;
String drouge;
String direction;

WebServer server(80);


const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));

PCA9539 ioport(0x76);

SFE_UBLOX_GPS myGPS;
int RXD2 = 26;
int TXD2 = 27;

#define buzzer 12
#define pyro1 5
#define pyro2 4
#define pyro1S 36
#define pyro2S 39
#define led1 11
#define led2 12
#define batt 35


// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

int transmissionState = ERR_NONE;
bool transmitFlag = false;
volatile bool enableInterrupt = true;
volatile bool operationDone = false;
void setFlag(void) {
  if (!enableInterrupt) {
    return;
  }
  operationDone = true;
}

byte passw = 0x60;
byte errorData = 0x00;
byte setConfig = 0x10;
byte sensData = 0x20;
byte flightSensData = 0x21;  // uçuştaki datalar
byte flightSensStatData = 0x22;
byte sendTouchGrndData = 0x23;
byte sendTouchGrndDataStat = 0x24;
byte getConfigData = 0x30;
byte getPrshtCon = 0x50;
byte sendFrqConfig = 0x31;
byte sendPrshtCon = 0x51;

float loraFreq = 868.00;
byte outputPower = 22;
uint8_t pyroStat;
bool pyroStatArr[8];

float accelMs5611;
long accelMs5611TimerNew,accelMs5611TimerOld;
float speed;
float speedOld;
float altitude;
float altitudeOld;
int altitudeTimer;
bool pyroFire1 = 0;
bool pyroFire2 = 0;
bool pyroProt1 = 0;
bool pyroProt2 = 0;
bool machLock = 0;
bool groundMode = 0;
bool apfallSpeedStart = 0;
bool apfallSpeedStop = 0;
unsigned int apfallSpeedCount = 0;
bool mainfallSpeedStart = 0;
bool mainfallSpeedStop = 0;
unsigned int mainfallSpeedCount = 0;


struct flightData {
  float H3LIS331_data[3];
  float Ms5611_data[4];
  float bnoAccel[3];
  float bnoGyro[3];
  float bnoMag[3];
  float bnoRaw[3];
  long GPS_data[4];
  float GpsSpeed;
  int GPS_time[6];
  uint8_t SIV;
  //tmElements_t my_time;    // time elements structure
  long unixTime;  // a timestamp
  float battery;

  float maxAltitude;
  float minAltitude;
  float maxSpeed;
  float maxAccel[3];
  uint8_t maxSiv;
  unsigned long engineBurnTime;
  unsigned long apogeeTime;
  float apogeeFallSpeed;
  float mainFallSpeed;
  unsigned long descendTime;

  unsigned long flightTime;
};
flightData flightdata;


struct datas {
  byte byteArray[200];
  int counter = 0;
};
datas data;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 2;
baro_ms5611 MS5611(MS_CS);

uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;
String All_data1[10];
String All_data2[10];
String All_data3[10];
String All_data4[10];
String All_data_flash1[10];
String All_data_flash2[10];
String All_data_flash3[10];
String All_data_flash4[10];
String header = "200gAccX;200gAccY;200gAccZ;Temp;Press;Alt;Speed;Accelms5611;AccX;AccY;AccZ;GyrX;GyrY;GyrZ;MagX;MagY;MagZ;Yaw;Pitch;Roll;GpsLat;GpsLong;GpsAlt;GpsHız;SIV;Batt;PyroStat;GpsTime\n";
String headerStat = ";;;;;;;;;;;;;;;;;;;;;;;;;;;maxAltitude;maxSpeed;maxAccelX;maxAccelY;maxAccelZ;maxSiv;engineBurnTime;apogeeTime;apogeeFallSpeed;mainFallSpeed;descendTime\n";
String statData;
int hafiza = 0;
int i = 0;

//#define SPIFFS LITTLEFS

bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;
uint8_t dataCheck = 0;
bool writeStart = 0;
bool error = 0;
bool sendSensorData = 0;
bool send_frq_ConfigData = 0;
bool setConOk = 0;
bool sendPrchtConData = 0;

bool storageArrayWrite = 0;
bool setGpsTime = 0;
bool sendgroundData = 0;
bool sendgroundDataStat =0;
bool sendStat = 0;
bool configFlashCheck=0;
bool writeStatCheck = 0;
TaskHandle_t Task1;

int pushButton = 0;
unsigned long wifiModeTimer = 0;

void setup() {
  Serial.begin(115200);
  setPins();
  configFlash();
  configLora();
  configGps();
  configH3lis();
  configMs5611();
  configBno();
  configScreen();
  buzzerToggle(5,50);
  wifiMode();
  
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */

  
}

void Task1code(void *pvParameters) {

  for (;;) {
    vTaskDelay(1);
    if (writeMode) {
      if(!configFlashCheck){
        appendFile(SPIFFS, "/Data.csv", header);
        configFlashCheck=1;
      } 
      flashWrite();
      
    }
  }

}

unsigned long start = 0;
unsigned long start2 = 0;
unsigned long start3 = 0;
unsigned long start4 = 0;

void loop() {
  vTaskDelay(1);

  if (!flightMode == 1) {

    preFlightAlgorithm();

  }else {
  
    readBno();
    if (millis() - start >= 90) {
      //Serial.printf("bno zamanı = %d \n",millis()-start3);
      start=millis();
      readH3lis();
      //start3=millis();
      readMs5611();
      //Serial.printf("ms5611 zamanı = %d \n",millis()-start3);
      //start3=millis();
      readGPS();
      //Serial.printf("gps zamanı = %d \n",millis()-start3);
      //start3=millis();
      readVoltage();
      //Serial.printf("voltage zamanı = %d \n",millis()-start3);
      //start3=millis(); 
      sendDataLora();
      //Serial.printf("lora zamanı = %d \n",millis()-start3);
      //start3=millis();
      writeData();
      //Serial.printf("write zamanı = %d \n",millis()-start3);
      flightAlgorithm();
    }
    
    //if(millis()-start2>80)Serial.printf("sensor zamanı        = %d \n",millis()-start2);
  }
}
void preFlightAlgorithm(){
  if (!groundMode) readBno();
    if (millis() - start >= 90) {
      start = millis();
      readGPS();
      if (!groundMode) {
        readH3lis();
        readMs5611();
        readVoltage();
        printData();
        String data;
        addDataString(data);
        if (vector.size() == vector.max_size()) {
          vector.remove(0);
        }
        vector.push_back(data);
      }
    }
    
    //if (altitude > 5 && ((-1.2 > flightdata.bnoAccel[0] || flightdata.bnoAccel[0] > 1.2) || (-1.2 > flightdata.bnoAccel[1] || flightdata.bnoAccel[1] > 1.2) || (-1.2 > flightdata.bnoAccel[2] || flightdata.bnoAccel[2] > 1.2))) {
      if (altitude > 5 ) {
      Serial.println("ucus modu");
      radio.clearDio1Action();
      flightMode = 1;
      writeMode = 1;
      storageArrayWrite = 1;
      //buzzerToggle(5,100);
      flightdata.apogeeTime = millis();
    } else if (operationDone) {
      enableInterrupt = false;
      operationDone = false;
      if (transmitFlag) {
        if (transmissionState == ERR_NONE) {
          Serial.println(F("transmission finished!"));
        } else {
          Serial.print(F("failed, code "));
          Serial.println(transmissionState);
        }
        radio.startReceive();
        transmitFlag = false;

      } else {
        int state = radio.readData(data.byteArray, 15);
        if (state == ERR_NONE) {
          Serial.println(F("[SX1262] Received packet!"));
          if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
            error = 0;
            send_frq_ConfigData = 1;
            data.counter = 2;
            loraFreq = byteToFloat(data.byteArray, &data.counter);
            outputPower = byteToByte(data.byteArray, &data.counter);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendFrqConfig) {
            error = 0;
            send_frq_ConfigData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == setConfig) {
            error = 0;
            setConOk = 1;
            radio.setFrequency(loraFreq);
            radio.setOutputPower(outputPower);
            EEPROM.writeFloat(4,loraFreq);
            EEPROM.writeByte(8,outputPower);
            EEPROM.commit();
          } else if (data.byteArray[0] == passw && data.byteArray[1] == getPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
            data.counter = 2;
            mainPar = byteToFloat(data.byteArray, &data.counter);
            EEPROM.writeFloat(0,mainPar);
            EEPROM.commit();
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sensData) {
            sendSensorData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendTouchGrndData) {
            sendgroundData = 1;
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendTouchGrndDataStat) {
            sendgroundDataStat = 1;
          }else {
            error = 1;
          }
        }
        Serial.print(F("[SX1262] Sending another packet ... "));
        if (error == 0 && send_frq_ConfigData == 1) {
          send_frq_ConfigData = 0;
        
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendFrqConfig, &data.counter, data.byteArray);
          floatAddByte(loraFreq, &data.counter, data.byteArray);
          byteAddByte(outputPower, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 7);
        } else if (error == 0 && sendPrchtConData == 1) {
          sendPrchtConData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendPrshtCon, &data.counter, data.byteArray);
          floatAddByte(mainPar, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 10);
        } else if (error == 0 && setConOk == 1) {
          setConOk = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(setConfig, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 2);
        } else if (error == 0 && sendSensorData == 1) {
          sendSensorData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sensData, &data.counter, data.byteArray);
          toByteAndStat();
          transmissionState = radio.startTransmit(data.byteArray, 70);
        } else if (error == 0 && sendgroundData == 1) {
          sendgroundData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendTouchGrndData, &data.counter, data.byteArray);
          for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);
          uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);
          floatAddByte(flightdata.battery, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 20);
        } else if (error == 0 && sendgroundDataStat == 1) {
          sendgroundDataStat = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendTouchGrndDataStat, &data.counter, data.byteArray);
          for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);
          uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);
          floatAddByte(flightdata.battery, &data.counter, data.byteArray);
          toByteStat();
          
          transmissionState = radio.startTransmit(data.byteArray, 50);
        } else {
          error = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          //hatali mesaj
          byteAddByte(errorData, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 15);
        }

        transmitFlag = true;
      }
      enableInterrupt = true;
    }
}

void flightAlgorithm(){
  if (altitude > 20 && !pyroProt1) {
      pyroProt1 = 1;
    }else if (pyroProt1 && !machLock && !pyroProt2) {
      
        
        if (flightdata.maxAltitude - altitude > 1) {
          pyro1Fire();
          pyroProt2 = 1;
          apfallSpeedStart = 1;
          sendStat = 0;
          flightdata.apogeeTime = millis() - flightdata.apogeeTime;
          flightdata.descendTime = millis();
          flightdata.minAltitude=flightdata.maxAltitude;
        }
      altitudeTimer = millis();
      altitudeOld = altitude;
      
    }else if (!groundMode && pyroProt2 && (altitude - mainPar) < 5) {
      
      pyro2Fire();
      apfallSpeedStop = 1;
      mainfallSpeedStart = 1;
      groundMode = 1;
      flightdata.apogeeFallSpeed = flightdata.apogeeFallSpeed / (apfallSpeedCount + 1);
      apfallSpeedStart = 0;

    }else if (groundMode) {
      
        if ((flightdata.minAltitude - altitude < 0) && ((-0.2 < flightdata.bnoGyro[0] || flightdata.bnoGyro[0] < 0.2) || (-0.2 < flightdata.bnoGyro[1] || flightdata.bnoGyro[1] < 0.2) || (-0.2 < flightdata.bnoGyro[2] || flightdata.bnoGyro[2] < 0.2))) {
          pyroProt1 = 0;
          pyroProt2 = 0;
          mainfallSpeedStop = 1;
          flightdata.mainFallSpeed = flightdata.mainFallSpeed / (mainfallSpeedCount + 1);
          mainfallSpeedStart = 0;
          flightdata.descendTime = millis() - flightdata.descendTime;
          flightdata.flightTime = flightdata.descendTime + flightdata.apogeeTime;
          statData =";;;;;;;;;;;;;;;;;;;;;;;;;;;"+String(flightdata.maxAltitude) + ";" + String(flightdata.maxSpeed) + ";" +
                   String(flightdata.maxAccel[0]) + ";" + String(flightdata.maxAccel[1]) + ";" + String(flightdata.maxAccel[2]) + ";" +
                   String(flightdata.maxSiv) + ";" + String(flightdata.engineBurnTime) + ";" + String(flightdata.apogeeTime) + ";" +
                   String(flightdata.apogeeFallSpeed) + ";" + String(flightdata.mainFallSpeed) + ";" + String(flightdata.descendTime) + "\n";
        statData.replace(".", ",");
        vTaskDelay(100);
        writeLastData();
        writeStatCheck=1;
        while(writeStatCheck) vTaskDelay(1);

        flightMode = 0;
        writeMode = 0;
        vTaskDelay(100);
        radio.setDio1Action(setFlag);
        Serial.print(F("[SX1262] Starting to listen ... "));
        radio.startReceive();
        
      }
      altitudeOld = altitude;
      altitudeTimer = millis();
      
    }

    if (apfallSpeedStart) {
      flightdata.apogeeFallSpeed += speed;
      apfallSpeedCount++;
    }
  
    if (mainfallSpeedStart) {
      flightdata.mainFallSpeed += speed;
      mainfallSpeedCount++;
    }
    
    float Gama = 1.4;
    float R = 287.05;
    float T = flightdata.Ms5611_data[0] + 273;  //Kelvine dönüştü
    //mach lock
    if (sqrt(Gama * R * T) <= (speed / 0.93969)) {
      machLock = 1;
    } else
      machLock = 0;
}

void configScreen(){
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    screenMode=0;
  }
  if(screenMode) display.clearDisplay();
}
void setPins() {
  configIOex();
  pinMode(BNO08X_CS, OUTPUT);
  pinMode(accel200gCSPin, OUTPUT);
  pinMode(MS_CS, OUTPUT);
  pinMode(buzzer, OUTPUT);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
  digitalWrite(buzzer, 0);
  pinMode(pushButton, INPUT_PULLUP);
}

void configLora() {
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(loraFreq, 500.0, 7, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
  radio.setCurrentLimit(110);
  radio.setDio1Action(setFlag);
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
}

void configFlash() {
  if(!SPIFFS.begin(1)){
    Serial.println("LITTLEFS Mount Failed");
    while(1);
  }
  //flashRead();
  //flashClean();
  flashCreatPart();
  //baslık dosyasını yaz
  vector.setStorage(storage_array);
  
  EEPROM.begin(512);

  if(EEPROM.readFloat(0)>0&&EEPROM.readFloat(4)>0&&EEPROM.readByte(8)>0){
    mainPar=EEPROM.readFloat(0);  //main paraşüt değiştirme
    loraFreq = EEPROM.readFloat(4);
    outputPower = EEPROM.readByte(8);
  }
}

void configGps() {
  do {
    Serial.println("GNSS: trying 38400 baud");
    Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) break;

    delay(100);
    Serial.println("GNSS: trying 9600 baud");
    Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) {
      Serial.println("GNSS: connected at 9600 baud, switching to 38400");
      myGPS.setSerialRate(38400);
      delay(100);
    } else {
      //myGNSS.factoryReset();
      delay(2000);  //Wait a bit before trying again to limit the Serial output
    }
  } while (1);
  Serial.println("GNSS serial connected");

  myGPS.setNMEAOutputPort(Serial);
  myGPS.saveConfiguration();  //Save the current settings to flash and BBR
  myGPS.setNavigationFrequency(10);
  rtc.setTime(0, 0, 0, 17, 2, 2022);
}


void configBno() {
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");
  //attachInterrupt(digitalPinToInterrupt(BNO08X_INT), interrupt_handler, FALLING);
  // enable interrupts right away to not miss first reports
  //interrupts();
  setReports();
  Serial.println("Reading events");
  delay(100);
  
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER, 90)) {
    Serial.println("Could not enable accelerometer");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED, 90)) {
    Serial.println("Could not enable gyroscope");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED, 90)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  delay(2);
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR, 90)) {
    Serial.println("Could not enable rotation vector");
  }
}


void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t *ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}

void configH3lis() {
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("H3lis start");
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
  //lis.enableHighPassFilter(1, LIS331_HPF_0_0025_ODR);
  lis.setLPFCutoff(LIS331_LPF_37_HZ);
}

void configMs5611() {
  MS5611.initialize();
  MS5611.calibrateAltitude();
}

void configIOex() {
  Wire.begin(21, 22);
  Wire.setClock(400000);
  ioport.pinMode(pyro1, OUTPUT);
  ioport.pinMode(pyro2, OUTPUT);
  ioport.pinMode(led1, OUTPUT);
  ioport.pinMode(led2, OUTPUT);
  delay(100);
}

void printData(void) {
  if(screenMode){
    printDataCount++;
    if(printDataCount>9){
      printDataCount=0;
      if(!digitalRead(pushButton)){
        //while(!digitalRead(pushButton));
        switchScreen++;
        if(switchScreen>4)switchScreen=0;
      }		
      if(switchScreen==0) printGPS();
      else if(switchScreen==1) printMs5611();
      else if(switchScreen==2) printBno1();
      else if(switchScreen==3) printBno2();
      else if(switchScreen==4) print400g();
    }
  }
}

void printGPS(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  String printGPS;

  printGPS ="GPS Time: "+String(rtc.getHour())+":"+String(rtc.getMinute())+":"+String(rtc.getSecond())+"\n"; 
  printGPS +="GPS Date: "+String(rtc.getDay())+"/"+String(rtc.getMonth()+1)+"/"+String(rtc.getYear())+"\n";
  printGPS +="GPS Vel : "+String(flightdata.GpsSpeed)+" m/s\n";
  printGPS +="GPS Lat : "+String(float(flightdata.GPS_data[0])/1000000,6)+"\n";
  printGPS +="GPS Long: "+String(float(flightdata.GPS_data[1])/1000000,6)+"\n";
  printGPS +="GPS Alt : "+String(float(flightdata.GPS_data[2])/1000)+" m\n";
  printGPS +="GPS Sat : "+String(flightdata.SIV);
  display.println(printGPS);
  display.display();
}
void printMs5611(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner

  String printGPS;
  printGPS  ="Temp     : "+String(flightdata.Ms5611_data[0])+"C\n";
  printGPS +="Altitude : "+String(flightdata.Ms5611_data[2])+" m\n";
  printGPS +="Velocity : "+String(flightdata.Ms5611_data[3])+" m/s\n";
  printGPS +="Battery  : "+String(flightdata.battery)+" V\n";
  printGPS +="Pyros    : "+String(pyroStatArr[0])+","+String(pyroStatArr[1])+"\n";
  printGPS +="Main Meter: "+String(mainPar)+" m\n";
  display.println(printGPS);
  display.display();
}
void printBno2(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  String printGPS;
  printGPS  ="Gyro-X : "+String(flightdata.bnoGyro[0])+" rad/s\n";
  printGPS +="Gyro-Y : "+String(flightdata.bnoGyro[1])+" rad/s\n";
  printGPS +="Gyro-Z : "+String(flightdata.bnoGyro[2])+" rad/s\n";
  printGPS +="Magno-X: "+String(flightdata.bnoMag[0])+" uT\n";
  printGPS +="Magno-Y: "+String(flightdata.bnoMag[1])+" uT\n";
  printGPS +="Magno-Z: "+String(flightdata.bnoMag[2])+" uT\n";
  display.println(printGPS);
  display.display();
}
void printBno1(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  String printGPS;
  printGPS  ="Accel-X: "+String(flightdata.bnoAccel[0])+" g\n";
  printGPS +="Accel-Y: "+String(flightdata.bnoAccel[1])+" g\n";
  printGPS +="Accel-Z: "+String(flightdata.bnoAccel[2])+" g\n";
  printGPS +="Yaw-X  : "+String(flightdata.bnoRaw[0])+"\n";
  printGPS +="Pitch-Y: "+String(flightdata.bnoRaw[1])+"\n";
  printGPS +="Roll-Z : "+String(flightdata.bnoRaw[2])+"\n";
  display.println(printGPS);
  display.display();
}
void print400g(){
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  String printGPS;
  printGPS  ="400g Acc-X: "+String(flightdata.H3LIS331_data[0])+" g\n";
  printGPS +="400g Acc-Y: "+String(flightdata.H3LIS331_data[1])+" g\n";
  printGPS +="400g Acc-Z: "+String(flightdata.H3LIS331_data[2])+" g\n";
  display.println(printGPS);
  display.display();
}

void readBno() {

  if (bno08x.wasReset()) {
    Serial.print("sensor was reset ");
    setReports();
  }

  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }



  switch (sensorValue.sensorId) {

    case SH2_ACCELEROMETER:
      flightdata.bnoAccel[0] = (sensorValue.un.accelerometer.x) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[1] = (sensorValue.un.accelerometer.y) / SENSORS_GRAVITY_STANDARD;
      flightdata.bnoAccel[2] = (sensorValue.un.accelerometer.z) / SENSORS_GRAVITY_STANDARD;

      break;
    case SH2_GYROSCOPE_CALIBRATED:
      flightdata.bnoGyro[0] = (sensorValue.un.gyroscope.x);
      flightdata.bnoGyro[1] = (sensorValue.un.gyroscope.y);
      flightdata.bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      flightdata.bnoMag[0] = (sensorValue.un.magneticField.x);
      flightdata.bnoMag[1] = (sensorValue.un.magneticField.y);
      flightdata.bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      flightdata.bnoRaw[0] = (ypr.yaw);
      flightdata.bnoRaw[1] = (ypr.pitch);
      flightdata.bnoRaw[2] = (ypr.roll);
      break;
  }
}

void readH3lis() {

  sensors_event_t event;
  lis.getEvent(&event);
  flightdata.H3LIS331_data[0] = (-event.acceleration.y / SENSORS_GRAVITY_STANDARD);
  flightdata.H3LIS331_data[1] = (event.acceleration.x / SENSORS_GRAVITY_STANDARD) ;
  flightdata.H3LIS331_data[2] = (event.acceleration.z / SENSORS_GRAVITY_STANDARD) ;
  digitalWrite(accel200gCSPin, 1);

}

void readMs5611() {

  MS5611.updateData();
  MS5611.updateCalAltitudeKalman();
  MS5611.updateSpeedKalman();
  flightdata.Ms5611_data[0] = MS5611.getTemperature_degC();
  flightdata.Ms5611_data[1] = MS5611.getPressure_mbar();
  flightdata.Ms5611_data[2] = MS5611.getAltitudeKalman();
  altitude = flightdata.Ms5611_data[2];
  flightdata.Ms5611_data[3] = speedKalmanFilter.updateEstimate(MS5611.getVelMs());
  speed = flightdata.Ms5611_data[3];
  accelMs5611TimerNew=millis();
  accelMs5611=((speed-speedOld)*1000)/(accelMs5611TimerNew-accelMs5611TimerOld);
  accelMs5611TimerOld=millis();
  accelMs5611 = accelKalmanFilter.updateEstimate(accelMs5611);
  speedOld = speed;
}
uint8_t byteToUint8t(byte *byterray, int *count) {
  uint8_t f;
  ((uint8_t *)&f)[i] = byterray[*count];
  return f;
}

float byteToFloat(byte *byterray, int *count) {
  float f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

long byteToLong(byte *byterray, int *count) {
  long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
unsigned long byteToUnsignedLong(byte *byterray, int *count) {
  unsigned long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count) {
  int f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt16(byte *byterray, int *count) {
  int16_t f;
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToUint16(byte *byterray, int *count) {
  uint16_t f;
  for (int i = 0; i < 2; i++) {
    ((uint8_t *)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count) {
  byte f;
  ((uint8_t *)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void unLongAddByte(unsigned long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void int16AddByte(int16_t  data, int *count, byte *array) {
  for (int i = 0; i < 2; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void unint16AddByte(uint16_t data, int *count, byte *array) {
  for (int i = 0; i < 2; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void uint8AddByte(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}

void toByteAndStat() {

  for (int k = 0; k < 3; k++) {
    int16AddByte(flightdata.H3LIS331_data[k]*100, &data.counter, data.byteArray);
    if (abs(flightdata.H3LIS331_data[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.H3LIS331_data[k];
  }

  //for (int k = 0; k < 2; k++) floatAddByte(flightdata.Ms5611_data[k], &data.counter, data.byteArray);

  int16AddByte(flightdata.Ms5611_data[0]*100, &data.counter, data.byteArray);
  floatAddByte(flightdata.Ms5611_data[2], &data.counter, data.byteArray);
  floatAddByte(flightdata.Ms5611_data[3], &data.counter, data.byteArray);

  if (flightdata.Ms5611_data[2] > flightdata.maxAltitude) flightdata.maxAltitude = flightdata.Ms5611_data[2];
  if (flightdata.Ms5611_data[2] < flightdata.minAltitude) flightdata.minAltitude = flightdata.Ms5611_data[2];
  if (abs(flightdata.Ms5611_data[3]) > abs(flightdata.maxSpeed)) flightdata.maxSpeed = flightdata.Ms5611_data[3];

  for (int k = 0; k < 3; k++) {
    int16AddByte(flightdata.bnoAccel[k]*100, &data.counter, data.byteArray);
    if (abs(flightdata.bnoAccel[k]) > abs(flightdata.maxAccel[k])) flightdata.maxAccel[k] = flightdata.bnoAccel[k];
  }

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoGyro[k]*100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoMag[k]*100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) int16AddByte(flightdata.bnoRaw[k]*100, &data.counter, data.byteArray);

  for (int k = 0; k < 3; k++) longAddByte(flightdata.GPS_data[k], &data.counter, data.byteArray);

  int16AddByte(flightdata.GpsSpeed*100, &data.counter, data.byteArray);
  
  longAddByte(flightdata.unixTime, &data.counter, data.byteArray);
  
  int16AddByte(flightdata.battery*100, &data.counter, data.byteArray);
  
  uint8AddByte(flightdata.SIV, &data.counter, data.byteArray);
  if (flightdata.SIV > flightdata.maxSiv) flightdata.maxSiv = flightdata.SIV;

  uint8AddByte(pyroStat, &data.counter, data.byteArray);
  
}

void toByteStat() {

  floatAddByte(flightdata.maxAltitude, &data.counter, data.byteArray);
  int16AddByte(flightdata.maxSpeed*50, &data.counter, data.byteArray);
  for (int k = 0; k < 3; k++) int16AddByte(flightdata.maxAccel[k]*100, &data.counter, data.byteArray);
  uint8AddByte(flightdata.maxSiv, &data.counter, data.byteArray);
  uint8AddByte(flightdata.engineBurnTime/1000, &data.counter, data.byteArray);
  unint16AddByte(flightdata.apogeeTime/1000, &data.counter, data.byteArray);
  if(apfallSpeedStop) floatAddByte(flightdata.apogeeFallSpeed, &data.counter, data.byteArray);
  else floatAddByte(0, &data.counter, data.byteArray);
  if(mainfallSpeedStop){
  floatAddByte(flightdata.mainFallSpeed, &data.counter, data.byteArray);
  unint16AddByte(flightdata.descendTime/1000, &data.counter, data.byteArray);
  }
  else {
    floatAddByte(0, &data.counter, data.byteArray);
    unint16AddByte(0, &data.counter, data.byteArray);
  }
}

void sendDataLora() {
  int size=70;
  data.counter = 0;
  byteAddByte(passw, &data.counter, data.byteArray);
  if(sendStat){
    byteAddByte(flightSensStatData, &data.counter, data.byteArray);
  }
  else{
    byteAddByte(flightSensData, &data.counter, data.byteArray);
  }
  
  toByteAndStat();
  if (sendStat) {
    toByteStat();
    size=100;
  }
  else{
    size=70;
  }
  
  int state = radio.transmit(data.byteArray, size);
  
  if (state == ERR_NONE) {

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packetgps
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
  }
  
}

void readGPS() {

  myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
  if (nmea.isValid() == true) {
    flightdata.GPS_data[0] = nmea.getLatitude();
    flightdata.GPS_data[1] = nmea.getLongitude();
    nmea.getAltitude(flightdata.GPS_data[2]);
    flightdata.GPS_data[3] = nmea.getSpeed();
    flightdata.GpsSpeed=(float(flightdata.GPS_data[3])/1000)*0.514444444;
    flightdata.SIV = nmea.getNumSatellites();
    
    if(!setGpsTime){
    rtc.setTime(nmea.getSecond(), nmea.getMinute(), nmea.getHour(), nmea.getDay(), nmea.getMonth(), nmea.getYear());
    flightdata.unixTime = rtc.getEpoch()+(timeZone*60*60);
    rtc.setTime(flightdata.unixTime,0);
    setGpsTime=1;
    }
    else{
    flightdata.unixTime = rtc.getEpoch();
    }
  }
}
void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void readVoltage() {
  flightdata.battery = readBatt();
  
  pyroStatArr[0] = pyro1Read();
  pyroStatArr[1] = pyro2Read();
  pyroStatArr[2] = pyroFire1;
  pyroStatArr[3] = pyroFire2;
  pyroStatArr[4] = machLock;
  
  pyroStat = 0;
  for (int i = 0; i < 5; i++) {
    pyroStat += pyroStatArr[i] * pow(2, i);
  }
  //Serial.println(pyroStat);
}

void pyro1Fire() {
  if (pyroProt1) {
    ioport.digitalWrite(pyro1, HIGH);
    pyroFire1 = 1;
  }
}
void pyro1Off() {
  ioport.digitalWrite(pyro1, LOW);
}

bool pyro1Read() {
  if ((analogRead(pyro1S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}

void pyro2Fire() {
  if (pyroProt2) {
    ioport.digitalWrite(pyro2, HIGH);
    pyroFire2 = 1;
  }
}
void pyro2Off() {

  ioport.digitalWrite(pyro2, LOW);
}
bool pyro2Read() {
  if ((analogRead(pyro2S) * 3.3 / 4095) * 6.6 > 2)
    return 1;
  else
    return 0;
}
void led1On() {
  ioport.digitalWrite(led1, HIGH);
}
void led1Off() {
  ioport.digitalWrite(led1, LOW);
}
void led2On() {
  ioport.digitalWrite(led2, HIGH);
}
void led2Off() {
  ioport.digitalWrite(led2, LOW);
}

void buzzerToggle(int loop,int delayms) {
  for(int p=0;p<loop;p++){
    digitalWrite(buzzer, HIGH);
    delay(delayms);
    digitalWrite(buzzer, LOW);
    delay(delayms);
  }
}

double readBatt() {
  double reading = analogRead(batt);  // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if (reading < 1 || reading > 4095) return 0;
  //return (-0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433)* 6.6;
  return (-0.000000000000016 * pow(reading, 4) + 0.000000000118171 * pow(reading, 3) - 0.000000301211691 * pow(reading, 2) + 0.001109019271794 * reading + 0.034143524634089) * 6.6;
}

void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == 9) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == 9) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void writeLastData() {
  if (dataCheck == 0) {
    //Serial.println(i);
    //for(i;i<10;i++) All_data1[i]="";
    flashWriteData(All_data1,i);
    file.close();
  } else if (dataCheck == 1) {
    
    //for(i;i<10;i++) All_data2[i]="";
    flashWriteData(All_data2,i);
    file.close();
  } else if (dataCheck == 2) {
    
    //for(i;i<10;i++) All_data3[i]="";
    flashWriteData(All_data3,i);
    file.close();
  } else if (dataCheck == 3) {

    //for(i;i<10;i++) All_data4[i]="";
    flashWriteData(All_data4,i);
    file.close();
  }

}

void addDataString(String &Data) {
  Data = String(flightdata.H3LIS331_data[0]) + ";" + String(flightdata.H3LIS331_data[1]) + ";" + String(flightdata.H3LIS331_data[2]) + ";";
  Data += String(flightdata.Ms5611_data[0]) + ";" + String(flightdata.Ms5611_data[1]) + ";" + String(flightdata.Ms5611_data[2]) + ";" + String(flightdata.Ms5611_data[3]) + ";" + String(accelMs5611)+ ";";
  Data += String(flightdata.bnoAccel[0]) + ";" + String(flightdata.bnoAccel[1]) + ";" + String(flightdata.bnoAccel[2]) + ";";
  Data += String(flightdata.bnoGyro[0]) + ";" + String(flightdata.bnoGyro[1]) + ";" + String(flightdata.bnoGyro[2]) + ";";
  Data += String(flightdata.bnoMag[0]) + ";" + String(flightdata.bnoMag[1]) + ";" + String(flightdata.bnoMag[2]) + ";";
  Data += String(flightdata.bnoRaw[0]) + ";" + String(flightdata.bnoRaw[1]) + ";" + String(flightdata.bnoRaw[2]) + ";";
  Data += String(flightdata.GPS_data[0]) + ";" + String(flightdata.GPS_data[1]) + ";" + String(flightdata.GPS_data[2]) + ";" + String(flightdata.GpsSpeed) + ";";
  Data += String(flightdata.SIV) + ";" +  String(flightdata.battery) + ";" +  String(pyroStat) + ";" + String(flightdata.unixTime) + "\n";
  Data.replace(".", ",");
}
   
void flashWrite() {

  if (storageArrayWrite) {
    storageArrayWrite = 0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < 10; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1,10);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2,10);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3,10);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4,10);
    writeCheck4 = 0;
  }
  if(writeStatCheck){
    writeStat();
    writeStatCheck=0;
  }
}

void writeStat(){
  appendFile(SPIFFS, "/Data.csv", headerStat);
  appendFile(SPIFFS, "/Data.csv", statData);
}

void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]) {
  file = SPIFFS.open("/Data.csv", FILE_APPEND);
  for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
    file.print(a[j]);
  }
  file.close();
}
void flashWriteData(String a[10],uint8_t wCounter) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.csv", FILE_APPEND);
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 9) {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < wCounter; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.csv");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.csv");
}
void flashCreatPart() {
  appendFile(SPIFFS, "/Data.csv", All_data_flash1[0]);
}


void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}
void wifiMode(){
  if(!digitalRead(pushButton)){
    wifiModeTimer=millis();
    while(!digitalRead(pushButton)){
      if(millis()-wifiModeTimer>2000){
        //buzzerToggle(2,500);
        led1On();
        writeMode = 0;
        ssid = "VODAFONE_70CD";
        password = "ulaseren9519";
        WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
        /*WiFi.softAP(ssid, password);
        IPAddress IP = WiFi.softAPIP();
        Serial.print("AP IP address: ");
        Serial.println(IP);  
*/
        if (!SPIFFS.begin(true)) {
        Serial.println("SPIFFS initialisation failed...");
        SPIFFS_present = false; 
        }
        else
        {
          Serial.println(F("SPIFFS initialised... file access enabled..."));
          SPIFFS_present = true; 
        }
        //----------------------------------------------------------------------   
        ///////////////////////////// Server Commands 
        server.on("/",         HomePage);
        server.on("/download", File_Download);
        server.on("/upload",   File_Upload);
        server.on("/fupload",  HTTP_POST,[](){ server.send(200);}, handleFileUpload);
        server.on("/delete",   File_Delete);
        server.on("/data",     Data);
        server.on("/settings", Settings);
        server.begin();
        Serial.println("HTTP server started");
        uint8_t ledTimer=0;
        bool ledStat=0;
        while(1) {
          delay(1);
          readBno();
          if (millis() - start >= 90) {
            start=millis();
            readH3lis();
            readMs5611();
            readGPS();
            readVoltage();
            if(ledTimer>9&&ledStat){
              led1On();
              ledStat=0;
              ledTimer=0;
            }
            else if(ledTimer>9&&!ledStat){
              led1Off();
              ledStat=1;
              ledTimer=0;
            }
            ledTimer++;
          }
          server.handleClient();
        }
      }
    }
  }
}
void append_page_header() {
  webpage  = F("<!DOCTYPE html><html>");
  webpage += F("<head>");
  webpage += F("<title>File Server</title>"); // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
  webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
  webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
  webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
  webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
  webpage += F("section {font-size:0.88em;}");
  webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
  webpage += F("h2{color:orange;font-size:1.0em;}");
  webpage += F("h3{font-size:0.8em;}");
  webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}"); 
  webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}"); 
  webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
  webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
  webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
  webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
  webpage += F(".column{float:left;width:50%;height:45%;}");
  webpage += F(".row:after{content:'';display:table;clear:both;}");
  webpage += F("*{box-sizing:border-box;}");
  webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
  webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
  webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
  webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
  webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
  webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
  webpage += F("a{font-size:75%;}");
  webpage += F("p{font-size:75%;}");
  webpage += F("</style></head><body><h1>File Server "); webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer(){ // Saves repeating many lines of code for HTML page footers
  webpage += F("<ul>");
  webpage += F("<li><a href='/'>Home</a></li>"); // Lower Menu bar command entries
  webpage += F("<li><a href='/download'>Download</a></li>"); 
  webpage += F("<li><a href='/upload'>Upload</a></li>"); 
  webpage += F("<li><a href='/delete'>Delete</a></li>"); 
  webpage += F("<li><a href='/data'>Data</a></li>");
  webpage += F("<li><a href='/settings'>Data</a></li>");
  webpage += F("</ul>");
  webpage += "<footer>&copy;"+String(char(byte(0x40>>1)))+String(char(byte(0x88>>1)))+String(char(byte(0x5c>>1)))+String(char(byte(0x98>>1)))+String(char(byte(0x5c>>1)));
  webpage += String(char((0x84>>1)))+String(char(byte(0xd2>>1)))+String(char(0xe4>>1))+String(char(0xc8>>1))+String(char(byte(0x40>>1)));
  webpage += String(char(byte(0x64/2)))+String(char(byte(0x60>>1)))+String(char(byte(0x62>>1)))+String(char(0x70>>1))+"</footer>";
  webpage += F("</body></html>");
}

void HomePage(){
  SendHTML_Header();
  webpage += F("<a href='/download'><button>Download</button></a>");
  webpage += F("<a href='/upload'><button>Upload</button></a>");
  webpage += F("<a href='/delete'><button>Delete</button></a>");
  webpage += F("<a href='/data'><button>Data</button></a>");
  webpage += F("<a href='/settings'><button>Settings</button></a>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Download(){ // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("download")) DownloadFile(server.arg(0));
  }
  else SelectInput("Enter filename to download","download","download");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename){
  if (SPIFFS_present) { 
    File download = SPIFFS.open("/"+filename,  "r");
    if (download) {
      server.sendHeader("Content-Type", "text/text");
      server.sendHeader("Content-Disposition", "attachment; filename="+filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else ReportFileNotPresent("download"); 
  } else ReportSPIFFSNotPresent();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Upload(){
  append_page_header();
  webpage += F("<h3>Select File to Upload</h3>"); 
  webpage += F("<FORM action='/fupload' method='post' enctype='multipart/form-data'>");
  webpage += F("<input class='buttons' style='width:40%' type='file' name='fupload' id = 'fupload' value=''><br>");
  webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Upload File</button><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  server.send(200, "text/html",webpage);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
File UploadFile; 
void handleFileUpload(){ // upload a new file to the Filing system
  HTTPUpload& uploadfile = server.upload(); // See https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer/srcv
                                            // For further information on 'status' structure, there are other reasons such as a failed transfer that could be used
  if(uploadfile.status == UPLOAD_FILE_START)
  {
    String filename = uploadfile.filename;
    if(!filename.startsWith("/")) filename = "/"+filename;
    Serial.print("Upload File Name: "); Serial.println(filename);
    SPIFFS.remove(filename);                  // Remove a previous version, otherwise data is appended the file again
    UploadFile = SPIFFS.open(filename, "w");  // Open the file for writing in SPIFFS (create it, if doesn't exist)
  }
  else if (uploadfile.status == UPLOAD_FILE_WRITE)
  {
    if(UploadFile) UploadFile.write(uploadfile.buf, uploadfile.currentSize); // Write the received bytes to the file
  } 
  else if (uploadfile.status == UPLOAD_FILE_END)
  {
    if(UploadFile)          // If the file was successfully created
    {                                    
      UploadFile.close();   // Close the file again
      Serial.print("Upload Size: "); Serial.println(uploadfile.totalSize);
      webpage = "";
      append_page_header();
      webpage += F("<h3>File was successfully uploaded</h3>"); 
      webpage += F("<h2>Uploaded File Name: "); webpage += uploadfile.filename+"</h2>";
      webpage += F("<h2>File Size: "); webpage += file_size(uploadfile.totalSize) + "</h2><br>"; 
      append_page_footer();
      server.send(200,"text/html",webpage);
    } 
    else
    {
      ReportCouldNotCreateFile("upload");
    }
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void printDirectory(const char * dirname, uint8_t levels){
  File root = SPIFFS.open(dirname);
  if(!root){
    return;
  }
  if(!root.isDirectory()){
    return;
  }
  File file = root.openNextFile();
  while(file){
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if(file.isDirectory()){
      webpage += "<tr><td>"+String(file.isDirectory()?"Dir":"File")+"</td><td>"+String(file.name())+"</td><td></td></tr>";
      printDirectory(file.name(), levels-1);
    }
    else
    {
      webpage += "<tr><td>"+String(file.name())+"</td>";
      webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024)                     fsize = String(bytes)+" B";
      else if(bytes < (1024 * 1024))        fsize = String(bytes/1024.0,3)+" KB";
      else if(bytes < (1024 * 1024 * 1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
      else                                  fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
      webpage += "<td>"+fsize+"</td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}

void Data(){
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h3>System Information</h3>";
  webpage += "<h4>Bno080 Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(flightdata.bnoAccel[0]) + "</td><td>" + String(flightdata.bnoAccel[1]) + "</td><td>"+ String(flightdata.bnoAccel[2]) +"</td></tr> ";
  webpage += "<tr><td>Gyro</td><td>"+ String(flightdata.bnoGyro[0]) + "</td><td>" + String(flightdata.bnoGyro[1]) + "</td><td>"+ String(flightdata.bnoGyro[2]) +"</td></tr> ";
  webpage += "<tr><td>Magneto</td><td>"+ String(flightdata.bnoMag[0]) + "</td><td>" + String(flightdata.bnoMag[1]) + "</td><td>"+ String(flightdata.bnoMag[2])+"</td></tr> ";
  webpage += "<tr><td>Euler</td><td>"+ String(flightdata.bnoRaw[0]) + "</td><td>" + String(flightdata.bnoRaw[1]) + "</td><td>"+  String(flightdata.bnoRaw[2])+"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>H3lis331dl Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(flightdata.H3LIS331_data[0]) + "</td><td>" + String(flightdata.H3LIS331_data[1]) + "</td><td>"+ String(flightdata.H3LIS331_data[2]) +"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>MS5611</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Temp</th><th>Altitude</th><th>Velocity</th><tr>";
  webpage += "<tr><td>"+String(flightdata.Ms5611_data[0])+"</td><td>" + String(flightdata.Ms5611_data[2])+"</td><td>" + String(flightdata.Ms5611_data[3]) + "</td></tr>";
  webpage += "</table>";
  webpage += "<h4>GPS Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Latitude</th><th>Longitude</th><th>Altitude</th><th>Speed</th><th>SIV</th><tr>";
  webpage += "<tr><td>"+ String(float(flightdata.GPS_data[0])/1000000,6) + "</td><td>" + String(float(flightdata.GPS_data[1])/1000000,6) + "</td><td>"+ String(float(flightdata.GPS_data[2])/1000)+"</td><td>"+ String(flightdata.GpsSpeed)+"</td><td>"+ String(flightdata.SIV) +"</td></tr> ";
  webpage += "</table>";
  //webpage += "<h4></h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Time</th><th>Date</th><tr>";
  webpage += "<tr><td>" +String(rtc.getHour())+":"+String(rtc.getMinute())+":"+String(rtc.getSecond())+ "</td><td>"+ String(rtc.getDay())+"/"+String(rtc.getMonth()+1)+"/"+String(rtc.getYear()) +"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>Pyro Information</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Batt</th><th>Pyro 1</th><th>Pyro 2</th></tr>";
  webpage += "<tr><td>"+String(flightdata.battery)+"</td><td>"+String(pyroStatArr[0])+"</td><td>"+String(pyroStatArr[1])+"</td></tr>";
  webpage += "</table> ";
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
}
  

void Settings(){
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<form action='/settings' method='POST'>";                                                                  
  webpage += "<h3>Settings</h3>";   
  webpage += "<label for='drouge'>Drouge to:</label>";
  webpage += "<input type='number' name='drouge'>";
  webpage += " ("+drouge+")<br>";
  webpage += "<label for='apooge'>Apooge to:</label>";
  webpage += "<input type='number' name='apooge'>";
  webpage += " ("+apooge+")<br>";      
  webpage += "Pyro ("+direction+") ";                                 
  webpage += "<input type='radio' name='direction' value='ON'>";  
  webpage += "<label for='ON'>ON</label>";
  webpage += "<input type='radio' name='direction' value='OFF'>";
  webpage += "<label for='ON'>OFF</label><br>";
  webpage += "<input type='submit' value='Set!'>";
  webpage += "</form>";  
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Delete(){
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("delete")) SPIFFS_file_delete(server.arg(0));
  }
  else SelectInput("Select a File to Delete","delete","delete");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) { // Delete the file 
  if (SPIFFS_present) { 
    SendHTML_Header();
    //File dataFile = SPIFFS.open("/"+filename, "r"); // Now read data from SPIFFS Card 
    //if (dataFile)
    //{
      if (SPIFFS.remove("/"+filename)) {
        Serial.println(F("File deleted successfully"));
        webpage += "<h3>File '"+filename+"' has been erased</h3>"; 
        webpage += F("<a href='/delete'>[Back]</a><br><br>");
      }
      else
      { 
        webpage += F("<h3>File was not deleted - error</h3>");
        webpage += F("<a href='delete'>[Back]</a><br><br>");
      }
    //} else ReportFileNotPresent("delete");
    append_page_footer(); 
    SendHTML_Content();
    SendHTML_Stop();
  } else ReportSPIFFSNotPresent();
} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header(){
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
  server.sendHeader("Pragma", "no-cache"); 
  server.sendHeader("Expires", "-1"); 
  server.setContentLength(CONTENT_LENGTH_UNKNOWN); 
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves. 
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content(){
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop(){
  server.sendContent("");
  server.client().stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SelectInput(String heading1, String command, String arg_calling_name){
  SendHTML_Header();
  webpage += F("<h3>"); webpage += heading1 + "</h3>"; 
  webpage += F("<FORM action='/"); webpage += command + "' method='post'>"; // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='"); webpage += arg_calling_name; webpage += F("' value=''><br>");
  webpage += F("<type='submit' name='"); webpage += arg_calling_name; webpage += F("' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th style='width:20%'>Type File/Dir</th><th>File Size</th></tr>");
  printDirectory("/",0);
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent(){
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>"); 
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target){
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportCouldNotCreateFile(String target){
  SendHTML_Header();
  webpage += F("<h3>Could Not Create Uploaded File (write-protected?)</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String file_size(int bytes){
  String fsize = "";
  if (bytes < 1024)                 fsize = String(bytes)+" B";
  else if(bytes < (1024*1024))      fsize = String(bytes/1024.0,3)+" KB";
  else if(bytes < (1024*1024*1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
  else                              fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
  return fsize;
}
struct Data{
byte byteArray[100];
int counter=0;
};


void setup() {
Serial.begin(115200);
Data data;
  
  floatAddByte(355.74,&data.counter,data.byteArray);
  for(int j=0;j<data.counter;j++){
     Serial.printf("byte %d is %02x\n", j, data.byteArray[j]);
  }
  longAddByte(1267567,&data.counter,data.byteArray);
  longAddByte(35556756,&data.counter,data.byteArray);
  for(int j=0;j<data.counter;j++){
     Serial.printf("byte %d is %02x\n", j, data.byteArray[j]);
  }
  byteAddByte(6,&data.counter,data.byteArray);
  byteAddByte(7,&data.counter,data.byteArray);
  for(int j=0;j<data.counter;j++){
     Serial.printf("byte %d is %02x\n", j, data.byteArray[j]);
  }
}

void loop() {
  
}
void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}

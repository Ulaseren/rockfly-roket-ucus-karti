/*
   RadioLib SX126x Transmit with Interrupts Example

   This example transmits LoRa packets with one second delays
   between them. Each packet contains up to 256 bytes
   of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// save transmission state between loops
int transmissionState = ERR_NONE;
void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {  
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {  
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;  
  (*count)++;
}
void int16AddByte(int16_t data, int *count, byte *array) {
  for (int m = 0; m < 2; m++) {
    array[*count] = ((uint8_t *)&data)[m];
    (*count)++;
  }
}
struct Data {
  byte byteArray[200];
  int counter = 0;
};
Data data;
void setup() {

  //SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS);

  delay(1000);
  Serial.begin(115200);

  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }

  // set the function that will be called
  // when packet transmission is finished
  radio.setDio1Action(setFlag);

  // start transmitting the first packet
  Serial.print(F("[SX1262] Sending first packet ... "));
  data.counter=0;
  byteAddByte(0x60, &data.counter, data.byteArray);
  byteAddByte(0x22, &data.counter, data.byteArray);
  for(int a=0;a<34;a++){
  floatAddByte(150.12, &data.counter, data.byteArray);
  }
  floatAddByte(200.12, &data.counter, data.byteArray);

  state = radio.startTransmit(data.byteArray, 150);
  // you can transmit C-string or Arduino string up to
  // 256 characters long
  //transmissionState = radio.startTransmit("Hello World!");

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x67,
                      0x89, 0xAB, 0xCD, 0xEF};
    state = radio.startTransmit(byteArr, 8);
  */
}

// flag to indicate that a packet was sent
volatile bool transmittedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is transmitted by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }

  // we sent a packet, set the flag
  transmittedFlag = true;
}

void loop() {
  // check if the previous transmission finished
  if (transmittedFlag) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    transmittedFlag = false;

    if (transmissionState == ERR_NONE) {
      // packet was successfully sent
      Serial.println(F("transmission finished!"));

      // NOTE: when using interrupt-driven transmit method,
      //       it is not possible to automatically measure
      //       transmission data rate using getDataRate()

    } else {
      Serial.print(F("failed, code "));
      Serial.println(transmissionState);
    }

    // wait a second before transmitting again
    delay(100);

    // send another one
    Serial.print(F("[SX1262] Sending another packet ... "));

    // you can transmit C-string or Arduino string up to
    // 256 characters long
    transmissionState = radio.startTransmit(data.byteArray, 150);
    // you can also transmit byte array up to 256 bytes long
    /*
      byte byteArr[] = {0x01, 0x23, 0x45, 0x67,
                        0x89, 0xAB, 0xCD, 0xEF};
      int state = radio.startTransmit(byteArr, 8);
    */

    // we're ready to send more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }
}

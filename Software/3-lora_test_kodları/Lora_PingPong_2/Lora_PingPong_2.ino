/*
   RadioLib SX126x Ping-Pong Example

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);
// uncomment the following only on one
// of the nodes to initiate the pings
//#define INITIATING_NODE

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33, SPI2);

struct Data {
  byte byteArray[100];
  int counter = 0;
};
Data data;
// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

int apogee;
int droug;
int carrierFreq = 915;
int8_t outputPower = 20;
int8_t loraReSetup = 0;
bool setCal[8];
bool flightModeCheck = 0;
bool loraRX = 1;
bool writeMode = 0;
bool parameterStat = 0;
bool flashClean = 0;
bool pyroMode = 0;
bool loraSet = 0;
// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicate transmission or reception state
bool transmitFlag = true;


bool ilkData = 0;
void setup() {
  Serial.begin(115200);
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK, HSPI_MISO, HSPI_MOSI, HSPI_CS);
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  // carrier frequency:           915.0 MHz
  // bandwidth:                   500.0 kHz
  // spreading factor:            6
  // coding rate:                 5
  // sync word:                   0x34 (public network/LoRaWAN)
  // output power:                2 dBm
  // preamble length:             20 symbols
  int state = radio.begin(carrierFreq, 500.0, 6, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }
}

void loop() {
  if (transmitFlag) {
    
    int state = radio.receive(data.byteArray, 14);
    if (state == ERR_NONE) {
      // packet was successfully received
      Serial.println(F("success!"));
      if (ilkData == 1) {
        Serial.println(F("[SX1262] Data:\t\t"));
        for (int i = 0; i < 4; i++) {
          ((uint8_t*)&apogee)[i] = data.byteArray[i];
        }
        Serial.println(apogee);
        for (int i = 4; i < 8; i++) {
          ((uint8_t*)&droug)[i - 4] = data.byteArray[i];
        }
        Serial.println(droug);
        for (int i = 8; i < 12; i++) {
          ((uint8_t*)&carrierFreq)[i - 8] = data.byteArray[i];
        }
        Serial.println(carrierFreq);
        ((uint8_t*)&outputPower)[0] = data.byteArray[12];
        Serial.println(outputPower);

        ((uint8_t*)&loraReSetup)[0] = data.byteArray[13];
        Serial.println(loraReSetup);
        for (int i = 0; loraReSetup > 0; i++) {
          setCal[i] = loraReSetup % 2;
          loraReSetup = loraReSetup / 2;
        }
        flightModeCheck = setCal[0];
        loraRX = setCal[1];
        writeMode = setCal[2];
        parameterStat = setCal[3];
        flashClean = setCal[4];
        pyroMode = setCal[5];
        loraSet = setCal[6];

        if (loraSet == 1) {
          loraSet = 0;
          radio.setFrequency(carrierFreq);
          radio.setOutputPower(outputPower);
        }
      }
      ilkData = 1;
      transmitFlag = false;

    } else if (state == ERR_RX_TIMEOUT) {
      // timeout occurred while waiting for a packet
      //Serial.println(F("timeout!"));

    } else if (state == ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      Serial.println(F("CRC error!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
    }

    //delay(100);
  } else {

    Serial.print(F("[SX1262] Transmitting packet ... "));

    int state = radio.transmit("Hello World!");
    if (state == ERR_NONE) {
      // the packet was successfully transmitted
      Serial.println(F("success!"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packet
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
    }

    transmitFlag = true;
  }
}

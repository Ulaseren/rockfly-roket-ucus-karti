#include "FS.h"
#include "LITTLEFS.h"


#define SPIFFS LITTLEFS

float totalBytes;
float usedBytes;

String fileName[20];
String directoryName[20];
int countFileName = 0;
int countDirecName = 0;

void setup() {
  Serial.begin(115200);
  if(!SPIFFS.begin(1)){
    Serial.println("LITTLEFS Mount Failed");
    return;
  }
  float totalBytes;
  float usedBytes;
  totalBytes=SPIFFS.totalBytes();
  usedBytes=SPIFFS.usedBytes();
  Serial.print("totalBytes = ");
  Serial.println(totalBytes);
  Serial.print("usedBytes = ");
  Serial.println(usedBytes);
  Serial.print("flash fill rate=%");
  Serial.println((usedBytes/totalBytes)*100);

  getDir(SPIFFS, "/", 1);
  deleteDir();

  totalBytes=SPIFFS.totalBytes();
  usedBytes=SPIFFS.usedBytes();
  Serial.print("totalBytes = ");
  Serial.println(totalBytes);
  Serial.print("usedBytes = ");
  Serial.println(usedBytes);
  Serial.print("flash fill rate=%");
  Serial.println((usedBytes/totalBytes)*100);
}

void loop() {
  // put your main code here, to run repeatedly:

}

void removeDir(fs::FS &fs, const char * path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void deleteFile(fs::FS &fs, const char * path) {
  Serial.printf("Deleting file: %s\n", path);
  if (fs.remove(path)) {
    Serial.println("File deleted");
  } else {
    Serial.println("Delete failed");
  }
}

void getDir(fs::FS &fs, const char * dirname, uint8_t levels) {

  File root = fs.open(dirname);
  File file = root.openNextFile();

  while (file) {
    if (file.isDirectory()) {
      directoryName[countDirecName] = String(file.name());
      countDirecName++;
      if (levels) {
        getDir(fs, file.name(), levels - 1);
      }
    }
    else {
      fileName[countFileName] = String(file.name());
      ++countFileName;
    }
    file = root.openNextFile();
  }
}

void deleteDir() {
  for (int j = 0; j < countFileName; j++) {
    int str_len = fileName[j].length() + 1;
    char char_array[str_len];
    fileName[j].toCharArray(char_array, str_len);
    deleteFile(SPIFFS, char_array);
  }

  for (int i = 0; i < countDirecName; i++) {
    int str_len = directoryName[i].length() + 1;
    char char_array[str_len];
    directoryName[i].toCharArray(char_array, str_len);
    removeDir(SPIFFS, char_array);
  }
}


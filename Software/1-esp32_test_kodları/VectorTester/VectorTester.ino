#include <Streaming.h>
#include <Vector.h>


const long BAUD = 115200;

const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
const size_t DELAY = 500;

void setup()
{
  Serial.begin(BAUD);
  while (!Serial)
  {
  // wait for serial port to connect.
  }
  String a;
  String storage_array[ELEMENT_COUNT_MAX];
  Elements vector;
  vector.setStorage(storage_array);
  Serial << "vector.max_size(): " << vector.max_size() << endl;
  Serial << "vector.size(): " << vector.size() << endl;
  Serial << "vector:" << endl;
  Serial << vector << endl;
  delay(DELAY);
  String data="-0.54,0.20,2.15,31.36,997.04,0.03,-0.00,-1.00,0.00,0.00,0.00,7.69,-16.19,33.44,-162.10,-1.68,-179.96,409108488,292179472,173508,4,1639125427\n";
  
  for(int i =0 ;i<80;i++){
    //int a=micros();
    if(vector.size()==vector.max_size()){
      vector.remove(0);
    }
    vector.push_back(String(i)+data);
  //a=micros()-a;
  //Serial.print("ekleme suresi=");
  //Serial.println(a);
  }
  Serial << "vector.max_size(): " << vector.max_size() << endl;
  Serial << "vector.size(): " << vector.size() << endl;
  //Serial << "vector:" << endl;
  //Serial << vector << endl;
  Serial.println(storage_array[0]);
  // a=vector.front();
  // Serial << "vector:" << endl;
  // Serial << vector << endl;
  // Serial.println(a);
  // Serial << "vector.max_size(): " << vector.max_size() << endl;
  // Serial << "vector.size(): " << vector.size() << endl;
  // vector.remove(0);
  // Serial << "vector.remove(0):" << endl;
  // Serial << vector << endl;
  // vector.remove(0);
  // Serial << "vector.remove(0):" << endl;
  // Serial << vector << endl;
  // delay(DELAY);

  // String storage_array2[ELEMENT_COUNT_MAX];
  // Elements vector2(storage_array2);
  // vector2.push_back("1");
  // vector2.push_back("2");
  // vector2.push_back("4");
  // vector2.pop_back();
  // Serial << "vector2.max_size(): " << vector2.max_size() << endl;
  // Serial << "vector2.size(): " << vector2.size() << endl;
  // Serial << "vector2:" << endl;
  // Serial << vector2 << endl;
  // delay(DELAY);
  // Serial << "Print vector2 elements using iterators: ";
  // for (String element : vector2)
  // {
  //   Serial << element << " ";
  // }
  // Serial << endl;
  // delay(DELAY);

  // String storage_array3[ELEMENT_COUNT_MAX];
  // storage_array3[0] = "3";
  // storage_array3[1] = "5";
  // Elements vector3(storage_array3);
  // Serial << "vector3.max_size(): " << vector3.max_size() << endl;
  // Serial << "vector3.size(): " << vector3.size() << endl;
  // Serial << "vector3:" << endl;
  // Serial << vector3 << endl;
  // delay(DELAY);

  // String storage_array4[ELEMENT_COUNT_MAX];
  // storage_array4[0] = "3";
  // storage_array4[1] = "5";
  // Elements vector4(storage_array4,2);
  // Serial << "vector4.max_size(): " << vector4.max_size() << endl;
  // Serial << "vector4.size(): " << vector4.size() << endl;
  // Serial << "vector4:" << endl;
  // Serial << vector4 << endl;
  // delay(DELAY);

  // String storage_array5[1];
  // Elements vector5(storage_array5);
  // Serial << "vector5.max_size(): " << vector5.max_size() << endl;
  // Serial << "vector5.size(): " << vector5.size() << endl;
  // Serial << "vector5:" << endl;
  // Serial << vector5 << endl;
  // delay(DELAY);

  // String storage_array6[ELEMENT_COUNT_MAX];
  // Elements vector6(storage_array6);
  // vector6.assign(ELEMENT_COUNT_MAX-1,8);
  // Serial << "vector6:" << endl;
  // Serial << vector6 << endl;
  // delay(DELAY);
}

void loop()
{
  
}



#include "ESP32TimerInterrupt.h"

#define TIMER_INTERRUPT_DEBUG         1
#define _TIMERINTERRUPT_LOGLEVEL_     4

bool toggle1 = 0;

void IRAM_ATTR TimerHandler1(void){


  Serial.print("ITimer1 called, millis() = "); Serial.println(millis());
  toggle1=1;
}

#define TIMER1_INTERVAL_MS        20  

ESP32Timer ITimer1(1);

void setup()
{
  Serial.begin(115200);
  ITimer1.attachInterruptInterval(TIMER1_INTERVAL_MS * 1000, TimerHandler1);  
}

void loop()
{
  if(toggle1==1){
  Serial.print("interrupt, millis() = "); Serial.println(millis());
  toggle1=0;
  //delay(10);
  }
}

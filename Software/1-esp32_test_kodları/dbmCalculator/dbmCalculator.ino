/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/AnalogReadSerial
*/

float attenuators=30.00;
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  float x = (analogRead(A0)*5.00)/1023.00;
  float y = 89.76785 - 971.5866*x + 2090.427*x*x - 2016.627*x*x*x + 934.325*x*x*x*x - 167.3191*x*x*x*x*x;
  float signalPower=y+attenuators;
  // print out the value you read:
  Serial.println(signalPower);
  delay(1);        // delay in between reads for stability
}

/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/stepper-motor-esp32-web-server/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>


// Replace with your network credentials
// const char* ssid = "VODAFONE_70CD";
// const char* password = "ulaseren9519";
const char* ssid = "RockFLY";
const char* password = "123456ULAS";
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Search for parameters in HTTP POST request
const char* PARAM_INPUT_1 = "apooge";
const char* PARAM_INPUT_2 = "drouge";
const char* PARAM_INPUT_3 = "direction";
// Variables to save values from HTML form
String apooge;
String drouge;
String direction;
// Variable to detect whether a new request occurred
bool newRequest = false;

// HTML to build the web page
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
  <title>Stepper Motor</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <h1>Set Param</h1>
    <form action="/" method="POST">
      <label for="apooge">Apooge to:</label>
      <input type="number" name="apooge">
      <input type="submit" value="Set!"><br>

      <label for="drouge">Drouge to:</label>
      <input type="number" name="drouge">
      <input type="submit" value="Set!"><br>
      <h5>Pyro Set</h5>
      <input type="radio" name="direction" value="ON" checked>
      <label for="ON">ON</label>
      <input type="radio" name="direction" value="OFF">
      <label for="OFF">OFF</label><br><br><br>      
    </form>
</body>
</html>
)rawliteral";

// Initialize WiFi
void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.softAP(ssid, password);
  Serial.print("Connecting to WiFi ..");
 
  Serial.println(WiFi.softAPIP());
}

void setup() {
  Serial.begin(115200);

  initWiFi();

  // Web Server Root URL
  //server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
  //  request->send(200, "text/html", index_html);
  //});
  
  // Handle request (form)
  server.on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    for(int i=0;i<params;i++){
      AsyncWebParameter* p = request->getParam(i);
      if(p->isPost()){
        // HTTP POST input1 value (direction)
        if (p->name() == PARAM_INPUT_1) {
          if(p->value().toInt()>0){
          apooge = p->value().c_str();
          }
        }
        // HTTP POST input2 value (steps)
        if (p->name() == PARAM_INPUT_2) {
          if(p->value().toInt()>0){
            drouge = p->value().c_str();
          
          }
        }
        if (p->name() == PARAM_INPUT_3) {
          direction = p->value().c_str();
          
        }
        Serial.print("apooge set to: ");
        Serial.println(apooge);
        Serial.print("drouge set to: ");
        Serial.println(drouge);
        Serial.print("Direction set to: ");
        Serial.println(direction);
      }
    }
    request->send(200, "text/html", index_html);
    newRequest = true;
  });

  server.begin();
}

void loop() {
  // Check if there was a new request and move the stepper accordingly
  if (newRequest){
    if (drouge == "CW"){
      // Spin the stepper clockwise direction
     
    }
    else{
      // Spin the stepper counterclockwise direction
     
    }
    newRequest = false;
  }
}
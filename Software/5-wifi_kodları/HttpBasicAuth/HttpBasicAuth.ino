#include <WiFi.h>
#include <ESPmDNS.h>
//#include <ArduinoOTA.h>
#include <WebServer.h>

const char *ssid = "esp_32";
const char *password = "ulas123456";

WebServer server(80);

const char* www_username = "admin";
const char* www_password = "esp32";

void setup() {
  Serial.begin(115200);

  WiFi.softAP(ssid, password);
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  
  server.on("/", []() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
    server.send(200, "text/plain", "Login OK");
  });

  server.begin();

}

void loop() {
  //ArduinoOTA.handle();
  server.handleClient();
  delay(2);//allow the cpu to switch to other tasks
}

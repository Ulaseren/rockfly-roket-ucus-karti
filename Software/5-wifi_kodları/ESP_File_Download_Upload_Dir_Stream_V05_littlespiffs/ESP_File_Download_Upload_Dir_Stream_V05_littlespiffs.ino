
#include "LITTLEFS.h"
#include "FS.h"
#include <SPI.h>
#include <WiFi.h>              // Built-in
#include <WebServer.h>

#define ServerVersion "1.0"
String  webpage = "";
#define SPIFFS LITTLEFS
bool    SPIFFS_present = false;
//const char* ssid = "RockFLY2022";
//const char* password = "rockfly2022";
const char* ssid = "VODAFONE_70CD";
const char* password = "ulaseren9519";
// Variables to save values from HTML form
float drogue=50.1;

WebServer server(80);

void setup(void){
  Serial.begin(115200);
    while (!Serial);
  Serial.println(__FILE__);
  WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    /*
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);  
*/
  if (!SPIFFS.begin(true)) {
    Serial.println("SPIFFS initialisation failed...");
    SPIFFS_present = false; 
  }
  else
  {
    Serial.println(F("SPIFFS initialised... file access enabled..."));
    SPIFFS_present = true; 
  }
  //----------------------------------------------------------------------   
  ///////////////////////////// Server Commands 
  server.on("/",         HomePage);
  server.on("/download", File_Download);
  server.on("/delete",   File_Delete);
  server.on("/data",     Data);
  server.on("/settings", Settings);
  
  ///////////////////////////// End of Request commands
  server.begin();
  Serial.println("HTTP server started");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void loop(){
  server.handleClient(); // Listen for client connections
}
void append_page_header() {
  webpage  = F("<!DOCTYPE html><html>");
  webpage += F("<script>function change(value){document.getElementById('download').value=value;}</script>");
  webpage += F("<head>");
  webpage += F("<title>File Server</title>"); // NOTE: 1em = 16px
  webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
  webpage += F("<style>");
  webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
  webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
  webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
  webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
  webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
  webpage += F("section {font-size:0.88em;}");
  webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
  webpage += F("h2{color:orange;font-size:1.0em;}");
  webpage += F("h3{font-size:0.8em;}");
  webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}"); 
  webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}"); 
  webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
  webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
  webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
  webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
  webpage += F(".column{float:left;width:50%;height:45%;}");
  webpage += F(".row:after{content:'';display:table;clear:both;}");
  webpage += F("*{box-sizing:border-box;}");
  webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
  webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
  webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
  webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
  webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
  webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
  webpage += F("a{font-size:75%;}");
  webpage += F("p{font-size:75%;}");
  webpage += F("</style></head><body><h1>File Server "); webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer(){ // Saves repeating many lines of code for HTML page footers
  webpage += F("<ul>");
  webpage += F("<li><a href='/'>Home</a></li>"); // Lower Menu bar command entries
  webpage += F("<li><a href='/download'>Download</a></li>"); 
  //webpage += F("<li><a href='/upload'>Upload</a></li>"); 
  webpage += F("<li><a href='/delete'>Delete</a></li>"); 
  webpage += F("<li><a href='/data'>Data</a></li>");
  webpage += F("<li><a href='/settings'>settings</a></li>");
  webpage += F("</ul>");
  webpage += "<footer>&copy;appcent</footer>";
  webpage += F("</body></html>");
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// All supporting functions from here...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void HomePage(){
  SendHTML_Header();
  webpage += F("<a href='/download'><button>Download</button></a>");
  //webpage += F("<a href='/upload'><button>Upload</button></a>");
  webpage += F("<a href='/delete'><button>Delete</button></a>");
  webpage += F("<a href='/data'><button>Data</button></a>");
  webpage += F("<a href='/settings'><button>Settings</button></a>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Download(){ // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("download")) DownloadFile(server.arg(0));
  }
  else{ //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
  SendHTML_Header();
  //webpage += F("<h3>Enter filename to download</h3>"); 
  webpage += F("<FORM action='/download' method='post'>"); // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='hidden' name='download' id='download' value=''><br>");
  webpage += F("<type='submit' name='download' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
  printDirectoryDownload("/",0,"Download");
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void DownloadFile(String filename){
  if (SPIFFS_present) { 
    File download = SPIFFS.open("/"+filename,  "r");
    if (download) {
      server.sendHeader("Content-Type", "text/text");
      server.sendHeader("Content-Disposition", "attachment; filename="+filename);
      server.sendHeader("Connection", "close");
      server.streamFile(download, "application/octet-stream");
      download.close();
    } else ReportFileNotPresent("download"); 
  } else ReportSPIFFSNotPresent();
}
void printDirectory(const char * dirname, uint8_t levels){
  File root = SPIFFS.open(dirname);
  if(!root){
    return;
  }
  if(!root.isDirectory()){
    return;
  }
  File file = root.openNextFile();
  while(file){
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if(file.isDirectory()){
      webpage += "<tr><td>"+String(file.isDirectory()?"Dir":"File")+"</th><th>"+String(file.name())+"</td><td></td></tr>";
      printDirectory(file.name(), levels-1);
    }
    else
    {
      webpage += "<tr><td>"+String(file.name())+"</td>";
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024)                     fsize = String(bytes)+" B";
      else if(bytes < (1024 * 1024))        fsize = String(bytes/1024.0,3)+" KB";
      else if(bytes < (1024 * 1024 * 1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
      else                                  fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
      webpage += "<td>"+fsize+"</td></tr>";
    }
    file = root.openNextFile();
  }
  file.close();
}
void printDirectoryDownload(const char * dirname, uint8_t levels,const char * buttonName){
  File root = SPIFFS.open(dirname);
  if(!root){
    return;
  }
  if(!root.isDirectory()){
    return;
  }
  File file = root.openNextFile();
  while(file){
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if(file.isDirectory()){
      webpage += "<tr><td>"+String(file.isDirectory()?"Dir":"File")+"</th><th>"+String(file.name())+"</td><td></td></tr>";
      printDirectory(file.name(), levels-1);
    }
    else
    {
      webpage += "<tr><td>"+String(file.name())+ "</td>";//
      //webpage += "<td>"+String(file.isDirectory()?"Dir":"File")+"</td>";
      int bytes = file.size();
      String fsize = "";
      if (bytes < 1024)                     fsize = String(bytes)+" B";
      else if(bytes < (1024 * 1024))        fsize = String(bytes/1024.0,3)+" KB";
      else if(bytes < (1024 * 1024 * 1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
      else                                  fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
      webpage += "<td>"+fsize+"</td><td><center><input type='submit' onclick=\"change('"+String(file.name())+"');\" value='"+String(buttonName)+"'></center></td></tr>";

    }
    file = root.openNextFile();
  }
  file.close();
}
void Data(){
  SendHTML_Header();
  webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<h3>System Information</h3>";
  webpage += "<h4>Bno080 Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(0.11) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.99) +"</td></tr> ";
  webpage += "<tr><td>Gyro</td><td>"+ String(0.01) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.01) +"</td></tr> ";
  webpage += "<tr><td>Magneto</td><td>"+ String(10) + "</td><td>" + String(12) + "</td><td>"+ String(90)+"</td></tr> ";
  webpage += "<tr><td>Euler</td><td>"+ String(3.1) + "</td><td>" + String(2) + "</td><td>"+  String(180)+"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>H3lis331dl Data</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Data</th><th>X</th><th>Y</th><th>Z</th><tr>";
  webpage += "<tr><td>Accel</td><td>"+ String(0.11) + "</td><td>" + String(0.01) + "</td><td>"+ String(0.99) +"</td></tr> ";
  webpage += "</table>";
  webpage += "<h4>MS5611</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Temp</th><th>Press</th><tr>";
  webpage += "<tr><td>"+String(21)+"</td><td>" + String(22) + "</td></tr>";
  webpage += "</table>";
  webpage += "<h4>Pyro Information</h4>";
  webpage += "<table class='center'>";
  webpage += "<tr><th>Pyro 1</th><th>Pyro 2</th></tr>";
  webpage += "<tr><td>OK</td><td>OK</td></tr>";
  webpage += "</table> ";
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
}
String fname,lname;
void Settings(){
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("settings")) {
      SendHTML_Header();
      webpage += "<h3>Data has been save</h3>"; 
      webpage += F("<a href='/settings'>[Back]</a><br><br>");
      Serial.print("server.arg(0)=");
      drogue=server.arg(0).toFloat();
      Serial.println(server.arg(0));
      append_page_footer(); 
      SendHTML_Content();
      SendHTML_Stop();
    }
    
  }
  else{
  SendHTML_Header();
  //webpage += "<meta http-equiv='refresh' content='5'>";
  webpage += "<form action='/settings' method='post'><br><br>";   
  webpage += "<label for='fdrogue'>Drogue:</label>";   
  webpage += "<input type='text' name='settings' id='settings' value='"+String(drogue)+"'><br><br><br>";
  webpage += "<button type='submit'>Submit</button><br><br>";
  webpage += "</form>";  
  append_page_footer(); 
  SendHTML_Content();
  SendHTML_Stop();
  }
  
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void File_Delete(){
  if (server.args() > 0 ) { // Arguments were received
    if (server.hasArg("delete")) SPIFFS_file_delete(server.arg(0));
  }
  else{ //SelectInput(String heading1, String command, String arg_calling_name) //SelectInput("Enter filename to download","download","download");
  SendHTML_Header();
  //webpage += F("<h3>Enter filename to download</h3>"); 
  webpage += F("<FORM action='/delete' method='post' onsubmit=\"return confirm('Do you really want to delete the file?');\"> "); // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='hidden' name='delete' id='download' value=''><br>");
  webpage += F("<type='submit' name='delete' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th>File Size</th><th></th></tr>");
  printDirectoryDownload("/",0,"Delete");
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
  }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SPIFFS_file_delete(String filename) { // Delete the file 
  if (SPIFFS_present) { 
    SendHTML_Header();
    //File dataFile = SPIFFS.open("/"+filename, "r"); // Now read data from SPIFFS Card 
    //if (dataFile)
    //{
      if (SPIFFS.remove("/"+filename)) {
        Serial.println(F("File deleted successfully"));
        webpage += "<h3>File '"+filename+"' has been erased</h3>"; 
        webpage += F("<a href='/delete'>[Back]</a><br><br>");
      }
      else
      { 
        webpage += F("<h3>File was not deleted - error</h3>");
        webpage += F("<a href='delete'>[Back]</a><br><br>");
      }
    //} else ReportFileNotPresent("delete");
    append_page_footer(); 
    SendHTML_Content();
    SendHTML_Stop();
  } else ReportSPIFFSNotPresent();
} 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Header(){
  server.sendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); 
  server.sendHeader("Pragma", "no-cache"); 
  server.sendHeader("Expires", "-1"); 
  server.setContentLength(CONTENT_LENGTH_UNKNOWN); 
  server.send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves. 
  append_page_header();
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Content(){
  server.sendContent(webpage);
  webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SendHTML_Stop(){
  server.sendContent("");
  server.client().stop(); // Stop is needed because no content length was sent
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void SelectInput(String heading1, String command, String arg_calling_name){
  SendHTML_Header();
  webpage += F("<h3>"); webpage += heading1 + "</h3>"; 
  webpage += F("<FORM action='/"); webpage += command + "' method='post'>"; // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='"); webpage += arg_calling_name; webpage += F("' value=''><br>");
  webpage += F("<type='submit' name='"); webpage += arg_calling_name; webpage += F("' value=''><br><br>");
  webpage += F("<a href='/'>[Back]</a><br><br>");
  webpage += F("<table align='center'>");
  webpage += F("<tr><th>Name/Type</th><th>File Size</th></tr>");
  printDirectory("/",0);
  webpage += F("</table><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportSPIFFSNotPresent(){
  SendHTML_Header();
  webpage += F("<h3>No SPIFFS Card present</h3>"); 
  webpage += F("<a href='/'>[Back]</a><br><br>");
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportFileNotPresent(String target){
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ReportCouldNotCreateFile(String target){
  SendHTML_Header();
  webpage += F("<h3>Could Not Create Uploaded File (write-protected?)</h3>"); 
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  append_page_footer();
  SendHTML_Content();
  SendHTML_Stop();
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
String file_size(int bytes){
  String fsize = "";
  if (bytes < 1024)                 fsize = String(bytes)+" B";
  else if(bytes < (1024*1024))      fsize = String(bytes/1024.0,3)+" KB";
  else if(bytes < (1024*1024*1024)) fsize = String(bytes/1024.0/1024.0,3)+" MB";
  else                              fsize = String(bytes/1024.0/1024.0/1024.0,3)+" GB";
  return fsize;
}

/*
   RadioLib SX126x Ping-Pong Example

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

float H3LIS331_data[3] = { 10, 20, 30 };
float Ms5611_data[2] = { 10, 20 };
float bnoAccel[3] = { 10, 20, 30 };
float bnoGyro[3] = { 10, 20, 30 };
float bnoMag[3] = { 10, 20, 30 };
float bnoRaw[3] = { 10, 20, 30 };
long GPS_data[3] = { 10, 20, 30 };
int GPS_time[6] = { 10, 20, 30, 10, 20, 30 };
uint8_t SIV = 3;

struct Data {
  byte byteArray[100];
  int counter = 0;
};
Data data;
// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

float mainPar;
float loraFreq = 868.00;
uint8_t outputPower = 20;
bool flightMode = 0;
float altitude;
float altitudeOld;
int altitudeTimer;
bool pyroProt1=0;
bool pyroProt2=0;
bool machLock=0;

byte passw = 0x60;
byte errorData = 0x00;
byte setConfig = 0x10;
byte sensData = 0x20;
byte flightsensData = 0x21; // uçuştaki datalar
byte getConfigData = 0x30;
byte getPrshtCon = 0x50;
byte sendFrqConfig = 0x31;
byte sendPrshtCon = 0x51;

bool error=0;
bool sendSensorData=0;
bool send_frq_ConfigData=0;
bool setConOk=0;
bool sendPrchtConData=0;
// save transmission states between loops
int transmissionState = ERR_NONE;

// flag to indicountte transmission or reception state
bool transmitFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// flag to indicountte that a packet was sent or received
volatile bool operationDone = false;

// this function is countlled when a complete packet
// is transmitted or received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enableInterrupt) {
    return;
  }

  // we sent aor received  packet, set the flag
  operationDone = true;
}

void setup() {
  Serial.begin(115200);
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(loraFreq, 500.0, 7, 5, 0x34, outputPower);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
  radio.setDio1Action(setFlag);
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }
}

void loop() {
  if (!flightMode) {
    //sensor okuma fonksiyonu HER AÇILDIĞINDA ALTİTUDE SIFIRLA
      if(altitude>20&&(-2.5>bnoAccel[0]>2.5||-2.5>bnoAccel[1]>2.5||-2.5>bnoAccel[2]>2.5)){
        radio.clearDio1Action();
        flightMode=1;
      }
    if (operationDone) {
      enableInterrupt = false;
      operationDone = false;
      if (transmitFlag) {
        if (transmissionState == ERR_NONE) {
          Serial.println(F("transmission finished!"));
        } else {
          Serial.print(F("failed, code "));
          Serial.println(transmissionState);
        }
        radio.startReceive();
        transmitFlag = false;

      } else {
        int state = radio.readData(data.byteArray, 15);
        if (state == ERR_NONE) {
          Serial.println(F("[SX1262] Received packet!"));
          if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
            error=0;
            send_frq_ConfigData=1;
            Serial.print("FREKANS data geldi ");
            data.counter=2;
            loraFreq= byteToFloat(data.byteArray,&data.counter);
            outputPower= byteToUint8t(data.byteArray,&data.counter);
            Serial.print("FREKANS ");
            Serial.println(loraFreq);
            Serial.print("DBM ");
            Serial.println(outputPower);
          }else if(data.byteArray[0] == passw && data.byteArray[1] == sendFrqConfig){
            error=0;
            send_frq_ConfigData=1;
            Serial.print("FREKANS data gönderildi ");
          }else if(data.byteArray[0] == passw && data.byteArray[1] == sendPrshtCon){
            error=0;
            sendPrchtConData=1;
            Serial.print("paraşüt data gönderildi ");            
            }else if(data.byteArray[0] == passw && data.byteArray[1] == setConfig){
            error=0;
            setConOk=1;
            Serial.print("FREKANS DEĞİŞTİ ");
            radio.setFrequency(loraFreq);
            radio.setOutputPower(outputPower);
          }else if(data.byteArray[0] == passw && data.byteArray[1] == getPrshtCon){
             error=0;
             sendPrchtConData=1;
             Serial.print("paraşüt data geldi ");
             data.counter=2;
             mainPar= byteToFloat(data.byteArray,&data.counter);           
             Serial.print("MAİN ");
             Serial.println(mainPar);
            }else if(data.byteArray[0] == passw && data.byteArray[1] == sensData){
            sendSensorData=1;
          }
          else{
            error=1;
          }
        }
        Serial.print(F("[SX1262] Sending another packet ... "));
        if(error==0&&send_frq_ConfigData==1){
          send_frq_ConfigData=0;
          Serial.println(" data gönderildi ");
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendFrqConfig, &data.counter, data.byteArray);
          floatAddByte(loraFreq, &data.counter, data.byteArray);
          byteAdduint8(outputPower, &data.counter, data.byteArray); 
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray,7); 
        }else if(error==0&&sendPrchtConData==1){
          sendPrchtConData=0;
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendPrshtCon, &data.counter, data.byteArray);
          floatAddByte(mainPar, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray,10);
          }else if(error==0&&setConOk==1){  
          setConOk=0;  
          data.counter=0; 
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(setConfig, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,2);      
        }else if(error==0&&sendSensorData==1){
          sendSensorData=0;
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sensData, &data.counter, data.byteArray);
          for (int k = 0; k < 3; k++) {
            floatAddByte(H3LIS331_data[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 2; k++) {
            floatAddByte(Ms5611_data[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoAccel[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoGyro[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoMag[k], &data.counter, data.byteArray);
          }
          for (int k = 0; k < 3; k++) {
            floatAddByte(bnoRaw[k], &data.counter, data.byteArray);
          } 
          for (int k = 0; k < 3; k++) {
            longAddByte(GPS_data[k], &data.counter, data.byteArray);
          }
          byteAddByte(SIV, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,90);
        }else{
          error=0;
          data.counter=0;
          byteAddByte(passw, &data.counter, data.byteArray);
          //hatali mesaj
          byteAddByte(errorData, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray,15);
        }
        
        transmitFlag = true;
      }
      enableInterrupt = true;
      
    }
  } else {
    //ucus kodları
    if(altitude>100&&!pyroProt1){
        pyroProt1=1;
      }
    
    if(pyroProt1&&!machLock&&!pyroProt2){
      if(millis()-altitudeTimer>=3000){

      if(altitude-altitudeOld<-5){
        //pyro1 aktiflestir
        pyroProt2=1;
      }
      altitudeOld=altitude;
      altitudeTimer=millis();
      }
    }
    if(pyroProt2&&(altitude-mainPar)<20){
        //pyro2 aktiflestir
      }
    
      //mach lock
    // if(){
    // }

    
  }
}

uint8_t byteToUint8t(byte *byterray,int *count)
{
  uint8_t f;
  for (int i = 0; i < 4; i++) {
  ((uint8_t*)&f)[i]=byterray[*count];
  (*count)++;
  }
return f;
  }

float byteToFloat(byte *byterray,int *count)
{
  float f;
  for (int i = 0; i < 4; i++) {
  ((uint8_t*)&f)[0]=byterray[*count];
  (*count)++;
  }
return f;
  }

  long byteToLong(byte *byterray,int *count)
{
  long f;
  for (int i = 0; i < 4; i++) {
  ((uint8_t*)&f)[0]=byterray[*count];
  (*count)++;
  }
return f;
  }
    int byteToInt(byte *byterray,int *count)
{
  int f;
  for (int i = 0; i < 4; i++) {
  ((uint8_t*)&f)[0]=byterray[*count];
   (*count)++;
  }
return f;
  }

    byte byteToByte(byte *byterray,int *count)
{
  byte f;
  ((uint8_t*)&f)[0]=byterray[*count];
  (*count)++;
  
return f;
  }

// void byteToFloat(float *data, int *count, byte *array) {
//   for (int i = 0; i < 4; i++) {
//     ((uint8_t *)&data)[i] = array[*count];
//     (*count)++;
//   }
// }

// void byteToInt(int *data, int *count, byte *array) {
//   for (int i = 0; i < 4; i++) {
//     ((uint8_t *)&data)[i] = array[*count];
//     (*count)++;
//   }
// }

// void byteToInt16(uint16_t *data, int *count, byte *array) {
//   for (int i = 0; i < 2; i++) {
//     ((uint8_t *)&data)[i] = array[*count];
//     (*count)++;
//   }
// }

// void byteToInt8(uint8_t *data, int *count, byte *array) {
//   ((uint8_t *)&data)[0] = array[*count];
//   (*count)++;
// }

void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}

void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}

void byteAdduint8(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}

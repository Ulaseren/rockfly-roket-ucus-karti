#include <SPI.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include "arduino_ms5611_spi.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include <Wire.h>
#include <RadioLib.h>
#include "SPI.h"
#include "SparkFun_Ublox_Arduino_Library.h"
#include <MicroNMEA.h>
#include "FS.h"
#include "LITTLEFS.h"

#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true
File file;

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 25;
int TXD2 = 26;
// SX1262 has the following connections:
// NSS pin:   12
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(12, 4, 32, 33);

void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
struct Data {
  byte byteArray[200];
  int counter = 0;
};
Data data;

String All_data1;
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Roll,Pitch,Yaw,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;
bool flashCheck1 = 0;
bool writeCheck1 = 0;
bool dataCheck = 0;

void flashWriteData(String a) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
      file.print(a);
      file.close();
    //hafiza++;
  }

  // else if (hafiza < 100) {
  //     file.print(a);
  //   hafiza++;
  // }

  else {
    file.print(a);
    file.close();
    hafiza = 0;
  }
}
void flashRead() {
  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {
  deleteFile(SPIFFS, "/Data.txt");
}
void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data1);
}


void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}

Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
uint8_t accel200gCSPin = 14;
BNO080 myIMU;
uint8_t imuCSPin = 5;
uint8_t imuWAKPin = -1;
uint8_t imuINTPin = 15;
uint8_t imuRSTPin = 2;
uint8_t BME_CS = 13;
#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme(BME_CS);
sensors_event_t event;

float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
uint8_t SIV;



void setup() {
  Serial.begin(115200);
  pinMode(imuCSPin, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(BME_CS, 1);
  digitalWrite(imuCSPin, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(BME_CS, 1);

  SPIFFS.begin(1);
  //flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();

  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }


   Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
   myGPS.begin(Serial2);
   myGPS.setSerialRate(38400);
   Serial.println("Trying 38400 baud");
   Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
   myGPS.begin(Serial2);
   //This will pipe all NMEA sentences to the serial port so we can see them
   myGPS.setNMEAOutputPort(Serial);
   myGPS.setNavigationFrequency(10);

  myIMU.beginSPI(imuCSPin, imuWAKPin, imuINTPin, imuRSTPin);
  myIMU.enableAccelerometer(20);  //Send data update every 50ms
  myIMU.enableGyro(20);           //Send data update every 50ms
  myIMU.enableMagnetometer(20);   //Send data update every 50ms
  myIMU.enableRotationVector(20);

  if (!lis.begin_SPI(14)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  Serial.println("H3LIS331 found!");
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);

  bme.begin();
}
String All_data;
unsigned long start = 0;
unsigned long start2 = 0;
unsigned long start3 = 0;
unsigned long end2 = 0;

 void SFE_UBLOX_GPS::processNMEA(char incoming) {
   nmea.process(incoming);
 }

void loop() {
  delay(1);
  myIMU.dataAvailable();
  if (millis() - start > 20) {
    start = millis();
    bnoAccel[0] = myIMU.getAccelX() / SENSORS_GRAVITY_STANDARD;
    bnoAccel[1] = myIMU.getAccelY() / SENSORS_GRAVITY_STANDARD;
    bnoAccel[2] = myIMU.getAccelZ() / SENSORS_GRAVITY_STANDARD;
    bnoGyro[0] = myIMU.getGyroX();
    bnoGyro[1] = myIMU.getGyroY();
    bnoGyro[2] = myIMU.getGyroZ();
    bnoMag[0] = myIMU.getMagX();
    bnoMag[1] = myIMU.getMagY();
    bnoMag[2] = myIMU.getMagZ();
    bnoRaw[0] = (myIMU.getRoll()) * 180.0 / PI;
    bnoRaw[1] = (myIMU.getPitch()) * 180.0 / PI;
    bnoRaw[2] = (myIMU.getYaw()) * 180.0 / PI;

    lis.getEvent(&event);
    H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;

    Ms5611_data[0] = bme.readTemperature();
    Ms5611_data[1] = bme.readPressure() / 100.0F;
    end2 = millis();
    if (end2 - start > 2) Serial.printf("okuma sensor: %d \n", end2 - start);

    if (millis() - start2 > 100) {
      start2 = millis();
      Serial.print(F("[SX1262] Transmitting packet ... "));

  // you can transmit C-string or Arduino string up to
  // 256 characters long
  // NOTE: transmit() is a blocking method!
  //       See example SX126x_Transmit_Interrupt for details
  //       on non-blocking transmission method.
  int state = radio.transmit("Hello World!");

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x56, 0x78, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
  */

  if (state == ERR_NONE) {
    // the packet was successfully transmitted
    Serial.println(F("success!"));

    // print measured data rate
    Serial.print(F("[SX1262] Datarate:\t"));
    Serial.print(radio.getDataRate());
    Serial.println(F(" bps"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));

  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Serial.println(F("timeout!"));

  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);

  }
      end2 = millis();
      if (end2 - start > 2) Serial.printf("okuma lora: %d \n", end2 - start2);

      start3 = millis();
      myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
      if (nmea.isValid() == true) {
        GPS_data[0] = nmea.getLatitude();
        GPS_data[1] = nmea.getLongitude();
        nmea.getAltitude(GPS_data[2]);
        SIV = nmea.getNumSatellites();
        GPS_time[0] = nmea.getYear();
        GPS_time[1] = nmea.getMonth();
        GPS_time[2] = nmea.getDay();
        GPS_time[3] = nmea.getHour();
        GPS_time[4] = nmea.getMinute();
        GPS_time[5] = nmea.getSecond();
      }
      end2 = millis();
      if (end2 - start > 2) Serial.printf("okuma GPS: %d \n", end2 - start3);
    }
    All_data1 = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data1 += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data1 += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
    All_data1 += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
    All_data1 += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
    All_data1 += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
    All_data1 += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data1 += String(SIV) + ",";
    All_data1 += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data1 += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
    start3 = millis();
    flashWriteData(All_data1);
    end2 = millis();
    Serial.printf("okuma suresi flash: %d \n", end2 - start3);
    end2 = millis();
    if (end2 - start > 2) Serial.printf("okuma suresi toplam: %d \n", end2 - start);
  }
}

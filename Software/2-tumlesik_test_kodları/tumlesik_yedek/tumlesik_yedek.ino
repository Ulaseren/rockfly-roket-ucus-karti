#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "MS5611.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
#include "FS.h"
#include "SPIFFS.h"

Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
SFE_UBLOX_GPS myGPS;
MS5611 MS5611(0x77);   // 0x76 = CSB to VCC; 0x77 = CSB to GND

long lastTime = 0; //Simple local timer. Limits amount if I2C traffic to Ublox module.

sensors_event_t event;
float H3LIS331_data[3];
float Ms5611_data[2];
long GPS_data[3];
int GPS_time[6];
byte SIV;

String All_data;
String header;
int i=0;

#define FORMAT_SPIFFS_IF_FAILED true

void readFile(fs::FS &fs, const char * path){
    
    Serial.printf("Reading file: %s\r\n", path);

    File file = fs.open(path);
    if(!file || file.isDirectory()){
        Serial.println("- failed to open file for reading");
        return;
    }

    Serial.println("- read from file:");
    while(file.available()){
        Serial.write(file.read());
    }

    file.close();
    
}

void writeFile(fs::FS &fs, const char * path, String &message){

    File file = fs.open(path, FILE_WRITE);
    file.print(message);
    file.close();

}

void appendFile(fs::FS &fs, const char * path, String &message){

    File file = fs.open(path, FILE_APPEND);
    file.print(message);
    file.close();

}

void deleteFile(fs::FS &fs, const char * path){

    Serial.printf("Deleting file: %s\r\n", path);
    if(fs.remove(path)){
        Serial.println("- file deleted");
    } else {
        Serial.println("- delete failed");
    }

}
void setup() {
  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(400000);

  MS5611.begin();
  lis.begin_I2C();   // change this to 0x19 for alternative i2c address
  myGPS.begin();

  lis.setRange(H3LIS331_RANGE_100_G);   // 100, 200, or 400 G!
  /*
  Serial.print("Range set to: ");
  switch (lis.getRange()) {
    case H3LIS331_RANGE_100_G: Serial.println("100 g"); break;
    case H3LIS331_RANGE_200_G: Serial.println("200 g"); break;
    case H3LIS331_RANGE_400_G: Serial.println("400 g"); break;
  }
  */
 lis.setDataRate(LIS331_DATARATE_50_HZ);
 /*
  Serial.print("Data rate set to: ");
  switch (lis.getDataRate()) {
    case LIS331_DATARATE_POWERDOWN: Serial.println("Powered Down"); break;
    case LIS331_DATARATE_50_HZ: Serial.println("50 Hz"); break;
    case LIS331_DATARATE_100_HZ: Serial.println("100 Hz"); break;
    case LIS331_DATARATE_400_HZ: Serial.println("400 Hz"); break;
    case LIS331_DATARATE_1000_HZ: Serial.println("1000 Hz"); break;
    case LIS331_DATARATE_LOWPOWER_0_5_HZ: Serial.println("0.5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_1_HZ: Serial.println("1 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_2_HZ: Serial.println("2 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_5_HZ: Serial.println("5 Hz Low Power"); break;
    case LIS331_DATARATE_LOWPOWER_10_HZ: Serial.println("10 Hz Low Power"); break;
  }
*/
  //GPS
  myGPS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGPS.setNavigationFrequency(10);
  myGPS.saveConfiguration();        //Save the current settings to flash and BBR
  if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
        Serial.println("SPIFFS Mount Failed");
        return;
    }

  readFile(SPIFFS, "/Data.txt");  
  deleteFile(SPIFFS, "/Data.txt");
  header="400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
  writeFile(SPIFFS, "/Data.txt", header);
}

void loop() {
  if ((millis() - lastTime > 20))
  {
  lastTime = millis(); //Update the timer
  i++;
  MS5611.read();
  lis.getEvent(&event);
  H3LIS331_data[0]=event.acceleration.x/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1]=event.acceleration.y/SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2]=event.acceleration.z/SENSORS_GRAVITY_STANDARD;
  Ms5611_data[0]=MS5611.getTemperature();
  Ms5611_data[1]=MS5611.getPressure();
  All_data=String(H3LIS331_data[0]);All_data+=",";
  All_data+=String(H3LIS331_data[1]);All_data+=",";
  All_data+=String(H3LIS331_data[2]);All_data+=",";
  All_data+=String(Ms5611_data[0]);All_data+=",";
  All_data+=String(Ms5611_data[1]);All_data+=",";
  
  //Serial.printf("X: %f,Y: %f,Z: %f g\n",H3LIS331_data[0], H3LIS331_data[1], H3LIS331_data[2]);
  //Serial.printf("T:%.2f,P:%.2f\n",Ms5611_data[0],Ms5611_data[1]);
  //GPS
  if (i==5)
  {
    i=0;
    //Serial.println("GPS");
    GPS_data[0] = myGPS.getLatitude();
    GPS_data[1] = myGPS.getLongitude();
    GPS_data[2] = myGPS.getAltitude();
    SIV = myGPS.getSIV();
    GPS_time[0]=myGPS.getYear();
    GPS_time[1]=myGPS.getMonth();
    GPS_time[2]=myGPS.getDay();
    GPS_time[3]=myGPS.getHour();
    GPS_time[4]=myGPS.getMinute();
    GPS_time[5]=myGPS.getSecond();
    
    All_data+=String(GPS_data[0]);All_data+=",";
    All_data+=String(GPS_data[1]);All_data+=",";
    All_data+=String(GPS_data[2]);All_data+=",";
    All_data+=String(SIV);All_data+=",";
    All_data+=String(GPS_time[0]);All_data+="-";
    All_data+=String(GPS_time[1]);All_data+="-";
    All_data+=String(GPS_time[2]);All_data+=",";
    All_data+=String(GPS_time[3]);All_data+=".";
    All_data+=String(GPS_time[4]);All_data+=".";
    All_data+=String(GPS_time[5]);All_data+="\n";
    //Serial.printf("Lat: %ld Long: %ld (degrees * 10^-7) Alt: %ld (mm) SIV: %02X \n",GPS_data[0],GPS_data[1],GPS_data[2],(int)SIV);
    //Serial.printf("%d-%02d-%02d %02d:%02d:%02d\n",GPS_time[0],GPS_time[1],GPS_time[2],GPS_time[3],GPS_time[4],GPS_time[5]);
  }
  else
  {
    All_data+=",,,,,\n";
  }
  appendFile(SPIFFS, "/Data.txt", All_data);
  //Serial.println(All_data);
  
  }

}

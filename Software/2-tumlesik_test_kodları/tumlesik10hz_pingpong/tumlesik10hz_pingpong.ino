#include "FS.h"
#include "LITTLEFS.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_H3LIS331.h>
#include "ms5611_spi.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO08x.h>
#include <Wire.h>
#include <RadioLib.h>
#include "SPI.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
#include <MicroNMEA.h>
#include "TimeLib.h"
#include "PCA9539.h"
#include <Vector.h>

const int ELEMENT_COUNT_MAX = 50;
typedef Vector<String> Elements;
String storage_array[ELEMENT_COUNT_MAX];
Elements vector;

char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));

PCA9539 ioport(0x76);

SFE_UBLOX_GPS myGPS;
int RXD2 = 26;
int TXD2 = 27;

#define pyro1 5
#define pyro2 4
#define pyro1S 39
#define pyro2S 36
#define led1 11
#define led2 12
#define batt 35
#define buzzer 12

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

int transmissionState = ERR_NONE;
bool transmitFlag = false;
volatile bool enableInterrupt = true;
volatile bool operationDone = false;
void setFlag(void) {
  if (!enableInterrupt) {
    return;
  }
  operationDone = true;
}

byte passw = 0x60;
byte errorData = 0x00;
byte setConfig = 0x10;
byte sensData = 0x20;
byte flightsensData = 0x21; // uçuştaki datalar
byte getConfigData = 0x30;
byte getPrshtCon = 0x50;
byte sendFrqConfig = 0x31;
byte sendPrshtCon = 0x51;

float mainPar;
float loraFreq = 868.00;
uint8_t outputPower = 20;
bool flightMode = 0;
float altitude;
float altitudeOld;
int altitudeTimer;
bool pyroProt1 = 0;
bool pyroProt2 = 0;
bool machLock = 0;

float H3LIS331_data[3];
float Ms5611_data[2];
float bnoAccel[3];
float bnoGyro[3];
float bnoMag[3];
float bnoRaw[3];
long GPS_data[3];
int GPS_time[6];
byte SIV=0;
tmElements_t my_time;          // time elements structure
unsigned long unixTime;  // a timestamp

struct Data {
  byte byteArray[200];
  int counter = 0;
};
Data data;

struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 2;
baro_ms5611 MS5611(MS_CS);

uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

File file;
String All_data1[10];
String All_data2[10];
String All_data3[10];
String All_data4[10];
String All_data_flash1[10];
String All_data_flash2[10];
String All_data_flash3[10];
String All_data_flash4[10];
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,MS5611Altitude,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Yaw,Pitch,Roll,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;

#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true

bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool flashCheck3 = 0;
bool flashCheck4 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool writeCheck3 = 0;
bool writeCheck4 = 0;
uint8_t dataCheck = 0;
bool writeStart = 0;
bool error = 0;
bool sendSensorData = 0;
bool send_frq_ConfigData = 0;
bool setConOk = 0;
bool sendPrchtConData = 0;
bool writeMode=0;
bool storageArrayWrite=0;
bool setGpsFreq=0;

TaskHandle_t Task1;

void setup() {
  Serial.begin(115200);
  
  setPins();
  configLora();
  configFlash();
  configGps();
  configBno();
  configH3lis();
  configMs5611();
  
  
  xTaskCreatePinnedToCore(
    Task1code, /* Task function. */
    "Task1",   /* name of task. */
    10000,     /* Stack size of task */
    NULL,      /* parameter of the task */
    1,         /* priority of the task */
    &Task1,    /* Task handle to keep track of created task */
    0);        /* pin task to core 0 */
    
}

void Task1code(void *pvParameters) {
  //Serial.print("Task1 running on core ");
  //Serial.println(xPortGetCoreID());

  for (;;) {
    vTaskDelay(1);
    if(writeMode){
      
      flashWrite();
    }
  }
}
unsigned long start = 0;
unsigned long start1 = 0;
unsigned long end = 0;

void loop() {
  delay(1);
  if (!flightMode) {
    readBno();
  if (millis() - start1 >= 100) {
    start1 = millis();
    readMs5611();
    readGPS();
    String data;
    addDataString(data);
    if(vector.size()==vector.max_size()){
      vector.remove(0);
    }
    vector.push_back(data);
  }
    if (altitude > 20 && (-2.5 > bnoAccel[0] > 2.5 || -2.5 > bnoAccel[1] > 2.5 || -2.5 > bnoAccel[2] > 2.5)) {
      radio.clearDio1Action();
      flightMode = 1;
      writeMode=1;
      storageArrayWrite=1;
    }
    else if (operationDone) {
      enableInterrupt = false;
      operationDone = false;
      if (transmitFlag) {
        if (transmissionState == ERR_NONE) {
          Serial.println(F("transmission finished!"));
        } else {
          Serial.print(F("failed, code "));
          Serial.println(transmissionState);
        }
        radio.startReceive();
        transmitFlag = false;

      } else {
        int state = radio.readData(data.byteArray, 15);
        if (state == ERR_NONE) {
          Serial.println(F("[SX1262] Received packet!"));
          if (data.byteArray[0] == passw && data.byteArray[1] == getConfigData) {
            error = 0;
            send_frq_ConfigData = 1;
            Serial.print("FREKANS data geldi ");
            data.counter = 2;
            loraFreq = byteToFloat(data.byteArray, &data.counter);
            outputPower = byteToUint8t(data.byteArray, &data.counter);
            Serial.print("FREKANS ");
            Serial.println(loraFreq);
            Serial.print("DBM ");
            Serial.println(outputPower);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendFrqConfig) {
            error = 0;
            send_frq_ConfigData = 1;
            Serial.print("FREKANS data gönderildi ");
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sendPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
            Serial.print("paraşüt data gönderildi ");
          } else if (data.byteArray[0] == passw && data.byteArray[1] == setConfig) {
            error = 0;
            setConOk = 1;
            Serial.print("FREKANS DEĞİŞTİ ");
            radio.setFrequency(loraFreq);
            radio.setOutputPower(outputPower);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == getPrshtCon) {
            error = 0;
            sendPrchtConData = 1;
            Serial.print("paraşüt data geldi ");
            data.counter = 2;
            mainPar = byteToFloat(data.byteArray, &data.counter);
            Serial.print("MAİN ");
            Serial.println(mainPar);
          } else if (data.byteArray[0] == passw && data.byteArray[1] == sensData) {
            sendSensorData = 1;
          }
          else {
            error = 1;
          }
        }
        Serial.print(F("[SX1262] Sending another packet ... "));
        if (error == 0 && send_frq_ConfigData == 1) {
          send_frq_ConfigData = 0;
          Serial.println(" data gönderildi ");
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendFrqConfig, &data.counter, data.byteArray);
          floatAddByte(loraFreq, &data.counter, data.byteArray);
          uint8AddByte(outputPower, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 7);
        } else if (error == 0 && sendPrchtConData == 1) {
          sendPrchtConData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sendPrshtCon, &data.counter, data.byteArray);
          floatAddByte(mainPar, &data.counter, data.byteArray);
          delay(100);
          transmissionState = radio.startTransmit(data.byteArray, 10);
        } else if (error == 0 && setConOk == 1) {
          setConOk = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(setConfig, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 2);
        } else if (error == 0 && sendSensorData == 1) {
          sendSensorData = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          byteAddByte(sensData, &data.counter, data.byteArray);
          toByte();
          transmissionState = radio.startTransmit(data.byteArray, 90);
        } else {
          error = 0;
          data.counter = 0;
          byteAddByte(passw, &data.counter, data.byteArray);
          //hatali mesaj
          byteAddByte(errorData, &data.counter, data.byteArray);
          transmissionState = radio.startTransmit(data.byteArray, 15);
        }

        transmitFlag = true;
      }
      enableInterrupt = true;

    }
  } else {
    //ucus kodları
    if (altitude > 100 && !pyroProt1) {
      pyroProt1 = 1;
    }

    if (pyroProt1 && !machLock && !pyroProt2) {
      if (millis() - altitudeTimer >= 3000) {

        if (altitude - altitudeOld < -5) {
          pyro1Fire();
          pyroProt2 = 1;
        }
        altitudeOld = altitude;
        altitudeTimer = millis();
      }
    }
    if (pyroProt2 && (altitude - mainPar) < 20) {
      pyro2Fire();
    }

    //mach lock
    // if(){
    // }
    readBno();
  if (millis() - start1 >= 100) {
    start1 = millis();
    readMs5611();
    sendDataLora();
    readGPS();
    start = millis();
    writeData();
    end = millis();
    //if (end - start > 10) Serial.printf("okuma suresi yazma: %d \n", end - start);
    //if (end - start1 > 30) Serial.printf("TOPLAM  yazma: %d \n", end - start1);
  }

  }
  
}

void setPins(){
  configIOex();
  pinMode(BNO08X_CS, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  pinMode(buzzer, 1);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1); 
  digitalWrite(buzzer, 0); 
}

void configLora(){
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true)
      ;
  }  
}

void configFlash(){
  SPIFFS.begin(1);
  //flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();  
  vector.setStorage(storage_array);
}

void configGps(){
   do {
    Serial.println("GNSS: trying 38400 baud");
    Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) break;

    delay(100);
    Serial.println("GNSS: trying 9600 baud");
    Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
    if (myGPS.begin(Serial2) == true) {
        Serial.println("GNSS: connected at 9600 baud, switching to 38400");
        myGPS.setSerialRate(38400);
        delay(100);
    } else {
        //myGNSS.factoryReset();
        delay(2000); //Wait a bit before trying again to limit the Serial output
    }
  } while(1);
  Serial.println("GNSS serial connected");

  myGPS.setNMEAOutputPort(Serial);
  myGPS.saveConfiguration(); //Save the current settings to flash and BBR
  myGPS.setNavigationFrequency(1);
}


void configBno(){
  Serial.println("Adafruit BNO08x test!");
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("Failed to find BNO08x chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x Found!");

  setReports();
  Serial.println("Reading events");
  delay(100);
  
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER,10)) {
    Serial.println("Could not enable accelerometer");
  }
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED,10)) {
    Serial.println("Could not enable gyroscope");
  }
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED,10)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR,10)) {
    Serial.println("Could not enable rotation vector");
  }
}

void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t* ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}

void configH3lis(){
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("Couldnt start");
    while (1) yield();
  }
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_100_HZ);
  lis.enableHighPassFilter(1,LIS331_HPF_0_0025_ODR);
  lis.setLPFCutoff(LIS331_LPF_37_HZ);
}

void configMs5611(){
  MS5611.initialize(); 
  MS5611.calibrateAltitude();
}

void configIOex(){
  Wire.begin(21,22);
  Wire.setClock(400000);
  ioport.pinMode(pyro1, OUTPUT);
  ioport.pinMode(pyro2, OUTPUT);
  ioport.pinMode(led1, OUTPUT);
  ioport.pinMode(led2, OUTPUT);
  delay(100);  
}

void readBno(){
  if (!bno08x.getSensorEvent(&sensorValue)) {
    return;
  }

  switch (sensorValue.sensorId) {
    //start = millis();
    case SH2_ACCELEROMETER:
      readH3lis();
      bnoAccel[0] =(sensorValue.un.accelerometer.x)/ SENSORS_GRAVITY_STANDARD;
      bnoAccel[1] =(sensorValue.un.accelerometer.y)/ SENSORS_GRAVITY_STANDARD;
      bnoAccel[2] =(sensorValue.un.accelerometer.z)/ SENSORS_GRAVITY_STANDARD;
      break;
    case SH2_GYROSCOPE_CALIBRATED:
      bnoGyro[0] = (sensorValue.un.gyroscope.x);
      bnoGyro[1] = (sensorValue.un.gyroscope.y);
      bnoGyro[2] = (sensorValue.un.gyroscope.z);
      break;
    case SH2_MAGNETIC_FIELD_CALIBRATED:
      bnoMag[0] = (sensorValue.un.magneticField.x);
      bnoMag[1] = (sensorValue.un.magneticField.y);
      bnoMag[2] = (sensorValue.un.magneticField.z);
      break;
    case SH2_ROTATION_VECTOR:
      quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
      bnoRaw[0] = (ypr.yaw);
      bnoRaw[1] = (ypr.pitch);
      bnoRaw[2] = (ypr.roll);
      break;
    //end = millis();
    //if (end - start > 3) Serial.printf("okuma suresi bno0 : %d \n", end - start);
  }
}

void readH3lis(){
  //start = millis();
  sensors_event_t event;
  lis.getEvent(&event);
  H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
  digitalWrite(accel200gCSPin, 1);
  //end = millis();
  //if (end - start > 3) Serial.printf("okuma suresi 400g : %d \n", end - start);
}

void readMs5611(){
  start = millis();
    MS5611.(); 
    MS5611.updateCalAltitudeKalman();
    Ms5611_data[0] = MS5611.getTemperature_degC();
    Ms5611_data[1] = MS5611.getPressure_mbar();
    altitude=MS5611.getAltitudeKalman();
    if(Ms5611_data[0]<0){
      Serial.printf("sicaklik %f, basinc %f\n", Ms5611_data[0],Ms5611_data[1]);
    }
    end = millis();
    if (end - start > 6) Serial.printf("okuma suresi ms5611  : %d \n", end - start);
}
uint8_t byteToUint8t(byte *byterray, int *count){
  uint8_t f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t*)&f)[i] = byterray[*count];
    (*count)++;
  }
  return f;
}

float byteToFloat(byte *byterray, int *count){
  float f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t*)&f)[0] = byterray[*count];
    (*count)++;
  }
  return f;
}

long byteToLong(byte *byterray, int *count){
  long f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t*)&f)[0] = byterray[*count];
    (*count)++;
  }
  return f;
}
int byteToInt(byte *byterray, int *count){
  int f;
  for (int i = 0; i < 4; i++) {
    ((uint8_t*)&f)[0] = byterray[*count];
    (*count)++;
  }
  return f;
}
byte byteToByte(byte *byterray, int *count){
  byte f;
  ((uint8_t*)&f)[0] = byterray[*count];
  (*count)++;
  return f;
}
void floatAddByte(float data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void longAddByte(long data, int *count, byte *array) {
  for (int i = 0; i < 4; i++) {
    array[*count] = ((uint8_t *)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void uint8AddByte(uint8_t data, int *count, byte *array) {
  array[*count] = data;
  (*count)++;
}
void toByte() {
    for (int k = 0; k < 3; k++) {
      floatAddByte(H3LIS331_data[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 2; k++) {
      floatAddByte(Ms5611_data[k], &data.counter, data.byteArray);
    }
    floatAddByte(altitude, &data.counter, data.byteArray);
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoAccel[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoGyro[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoMag[k], &data.counter, data.byteArray);
    }
    for (int k = 0; k < 3; k++) {
      floatAddByte(bnoRaw[k], &data.counter, data.byteArray);
    }

    for (int k = 0; k < 3; k++) {
      longAddByte(GPS_data[k], &data.counter, data.byteArray);
    }
    byteAddByte(SIV, &data.counter, data.byteArray);
    longAddByte(unixTime, &data.counter, data.byteArray);
    data.counter = 0;
  
}
void sendDataLora(){
  toByte();
  start = millis();
    int state = radio.transmit(data.byteArray,100);

    if (state == ERR_NONE) {
      // the packet was successfully transmitted
      //Serial.println(F("success!"));
      // print measured data rate
      // Serial.print(F("[SX1262] Datarate:\t"));
      // Serial.print(radio.getDataRate());
      // Serial.println(F(" bps"));

    } else if (state == ERR_PACKET_TOO_LONG) {
      // the supplied packet was longer than 256 bytes
      Serial.println(F("too long!"));

    } else if (state == ERR_TX_TIMEOUT) {
      // timeout occured while transmitting packetgps
      Serial.println(F("timeout!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);
    }
    end = millis();
    //if (end - start > 20) Serial.printf("okuma suresi Lora  : %d \n", end - start);
}
int gpsCount=0;
void readGPS(){
  
  start = millis();
  if(gpsCount>=9){
  myGPS.checkUblox();  //See if new data is available. Process bytes as they come in.
    if (nmea.isValid() == true||SIV>=4) {
      GPS_data[0] = nmea.getLatitude();
      GPS_data[1] = nmea.getLongitude();
      nmea.getAltitude(GPS_data[2]);
      SIV = nmea.getNumSatellites();
      GPS_time[0] = nmea.getYear();
      GPS_time[1] = nmea.getMonth();
      GPS_time[2] = nmea.getDay();
      GPS_time[3] = nmea.getHour();
      GPS_time[4] = nmea.getMinute();
      GPS_time[5] = nmea.getSecond();
      my_time.Year = GPS_time[0] - 1970;
      my_time.Month = GPS_time[1];
      my_time.Day = GPS_time[2];
      my_time.Hour = GPS_time[3];
      my_time.Minute = GPS_time[4];
      my_time.Second = GPS_time[5];
      unixTime = (long)(makeTime(my_time));
  gpsCount=10;
  }
  else if(4>SIV>=1){
    SIV  = nmea.getNumSatellites();
    GPS_time[0] = nmea.getYear();
      GPS_time[1] = nmea.getMonth();
      GPS_time[2] = nmea.getDay();
      GPS_time[3] = nmea.getHour();
      GPS_time[4] = nmea.getMinute();
      GPS_time[5] = nmea.getSecond();
      my_time.Year = GPS_time[0] - 1970;
      my_time.Month = GPS_time[1];
      my_time.Day = GPS_time[2];
      my_time.Hour = GPS_time[3];
      my_time.Minute = GPS_time[4];
      my_time.Second = GPS_time[5];
      unixTime = (long)(makeTime(my_time));
    gpsCount=0;
    if(SIV>=4&&!setGpsFreq){
      setGpsFreq=1;
      myGPS.setNavigationFrequency(10);
      gpsCount=10;
    }
  }
  else{
    SIV  = nmea.getNumSatellites();
    gpsCount=0;
    if(SIV>=4&&!setGpsFreq){
      setGpsFreq=1;
      myGPS.setNavigationFrequency(10);
      gpsCount=10;
    }
  }
  }
  else{
    gpsCount++;
  }
  
  end = millis();
  //if (end - start > 10) Serial.printf("okuma suresi GPS  : %d \n", end - start);
}
void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}
void pyro1Fire(){
  if(pyroProt1){
    ioport.digitalWrite(pyro1, HIGH);
  }
}
void pyro1Off(){
  ioport.digitalWrite(pyro1, LOW);
}
double pyro1Read(){
  return (analogRead(pyro1S)*3.3/4095)*6.6;
}
void pyro2Fire(){
  if(pyroProt2){
  ioport.digitalWrite(pyro2, HIGH);
  }
}
void pyro2Off(){
  ioport.digitalWrite(pyro2, LOW);
}
double pyro2Read(){
  return (analogRead(pyro2S)*3.3/4095)*6.6;
}
void led1On(){
  ioport.digitalWrite(led1, HIGH);
}
void led1Off(){
  ioport.digitalWrite(led1, LOW);
}
void led2On(){
  ioport.digitalWrite(led2, HIGH);
}
void led2Off(){
  ioport.digitalWrite(led2, LOW);
}
void buzzerOn(){
digitalWrite(buzzer, HIGH);
}
void buzzerOff(){
digitalWrite(buzzer, LOW);
}
double readBatt(){
  double reading = analogRead(batt); // Reference voltage is 3v3 so maximum reading is 3v3 = 4095 in range 0 to 4095
  if(reading < 1 || reading > 4095) return 0;
  // return -0.000000000009824 * pow(reading,3) + 0.000000016557283 * pow(reading,2) + 0.000854596860691 * reading + 0.065440348345433;
  return  (-0.000000000000016 * pow(reading,4) + 0.000000000118171 * pow(reading,3)- 0.000000301211691 * pow(reading,2)+ 0.001109019271794 * reading + 0.034143524634089)*6.6;
}

void writeData() {

  if (dataCheck == 0) {
    while (flashCheck1) {
      vTaskDelay(1);
    }
    addDataString(All_data1[i]);
    //Serial.println(All_data1[i]);
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    } else {
      i++;
    }
  } else if (dataCheck == 1) {
    while (flashCheck2) {
      vTaskDelay(1);
    }
    addDataString(All_data2[i]);
    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 2;
    } else {
      i++;
    }
  } else if (dataCheck == 2) {
    while (flashCheck3) {
      vTaskDelay(1);
    }
    addDataString(All_data3[i]);
    if (i == 9) {
      i = 0;
      flashCheck3 = 1;
      dataCheck = 3;
    } else {
      i++;
    }
  } else if (dataCheck == 3) {
    while (flashCheck4) {
      vTaskDelay(1);
    }
    addDataString(All_data4[i]);
    if (i == 9) {
      i = 0;
      flashCheck4 = 1;
      dataCheck = 0;
    } else {
      i++;
    }
  }
}

void addDataString(String &Data){
  Data = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
  Data += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ","+String(altitude)+ ",";
  Data += String(bnoAccel[0]) + "," + String(bnoAccel[1]) + "," + String(bnoAccel[2]) + ",";
  Data += String(bnoGyro[0]) + "," + String(bnoGyro[1]) + "," + String(bnoGyro[2]) + ",";
  Data += String(bnoMag[0]) + "," + String(bnoMag[1]) + "," + String(bnoMag[2]) + ",";
  Data += String(bnoRaw[0]) + "," + String(bnoRaw[1]) + "," + String(bnoRaw[2]) + ",";
  Data += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
  Data += String(SIV) + ","+ String(unixTime) + "\n";
}

void flashWrite() {

  if(storageArrayWrite){
    storageArrayWrite=0;
    flashWriteFirstData(storage_array);
  }
  if (flashCheck1 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (flashCheck3 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash3[j] = All_data3[j];
    }
    flashCheck3 = 0;
    writeCheck3 = 1;
  }

  if (flashCheck4 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash4[j] = All_data4[j];
    }
    flashCheck4 = 0;
    writeCheck4 = 1;
  }

  if (writeCheck1 == 1) {
    flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }

  if (writeCheck3 == 1) {
    flashWriteData(All_data_flash3);
    writeCheck3 = 0;
  }

  if (writeCheck4 == 1) {
    flashWriteData(All_data_flash4);
    writeCheck4 = 0;
  }
}

void flashWriteFirstData(String a[ELEMENT_COUNT_MAX]){
  file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < ELEMENT_COUNT_MAX; j++) {
      file.print(a[j]);
    }
  file.close();
}
void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 9) {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {
  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");
}
void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}


void listDir(fs::FS &fs, const char *dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print(file.name());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm *tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char *path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char *path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char *path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char *path) {
  fs.remove(path);
}

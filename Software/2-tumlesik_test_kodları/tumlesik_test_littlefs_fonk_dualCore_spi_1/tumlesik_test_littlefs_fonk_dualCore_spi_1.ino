#include <Wire.h>
#include <SPI.h>
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include "arduino_ms5611_spi.h"
#include "SparkFun_BNO080_Arduino_Library.h"
#include "SparkFun_Ublox_Arduino_Library.h" //http://librarymanager/All#SparkFun_u-blox_GNSS
#include <MicroNMEA.h> //http://librarymanager/All#MicroNMEA
#include "FS.h"
#include "LITTLEFS.h"
#include "ESP32TimerInterrupt.h"


#define SPIFFS LITTLEFS
#define FORMAT_LITTLEFS_IF_FAILED true

#define TIMER_INTERRUPT_DEBUG         1
#define _TIMERINTERRUPT_LOGLEVEL_     4


File file;

SFE_UBLOX_GPS myGPS;
char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));

//sensor
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();
sensors_event_t event;
BNO080 myIMU;
MS5611_SPI ms5611(13, 19, 23, 18, 2);
//Dual Core
TaskHandle_t Task1;
TaskHandle_t Task2;
//sensor data
float H3LIS331_data[3];
float Ms5611_data[2];
float x_Accel, y_Accel, z_Accel;
float x_Gyro, y_Gyro, z_Gyro;
float x_Mag, y_Mag, z_Mag;
float Roll, Pitch, Yaw;
long GPS_data[3];
int GPS_time[6];
uint8_t SIV;
String All_data1[10];
String All_data2[10];
String All_data_flash1[10];
String All_data_flash2[10];
String header = "400gAcceloX,400gAcceloY,400gAcceloZ,MS5611Temp,MS5611Pressure,BNOAccelX,BNOAccelY,BNOAccelZ,BNOGyroX,BNOGyroY,BNOGyroZ,BNOMagX,BNOMagY,BNOMagZ,Roll,Pitch,Yaw,GpsLatitude,GpsLongitude,GpsAltitude,GpsDate,GpsTime\n";
int hafiza = 0;
int i = 0;
bool count = 0;
int countGps = 0;
//hafıza
bool flashCheck1 = 0;
bool flashCheck2 = 0;
bool writeCheck1 = 0;
bool writeCheck2 = 0;
bool dataCheck = 0;
//timer
ESP32Timer ITimer1(1);

//fonksiyon prototipleri
void sensorBegin();
void readSensor();
void flashRead();
void flashClean();
void flashCreatPart();
void flashWrite();
void sensorStart();
void readH3LIS331();
void readBno080();
void readMS5611();
void readGPS();
void writeData();
void IRAM_ATTR TimerHandler1(void);
void listDir(fs::FS &fs, const char * dirname, uint8_t levels);
void createDir(fs::FS &fs, const char * path);
void removeDir(fs::FS &fs, const char * path);
void readFile(fs::FS &fs, const char * path);
void writeFile(fs::FS &fs, const char * path, String message);
void appendFile(fs::FS &fs, const char * path, String message);
void deleteFile(fs::FS &fs, const char * path);


void setup() {

  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(400000);
  sensorBegin();
  //hafıza baslat
  SPIFFS.begin(1);
  //flashRead();
  flashClean();
  //baslık dosyasını yaz
  writeFile(SPIFFS, "/Data.txt", header);
  flashCreatPart();
  //listDir(SPIFFS, "/", 0);
  //timer ayar
  timerBegin();

  //create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
  xTaskCreatePinnedToCore(
    Task1code,   /* Task function. */
    "Task1",     /* name of task. */
    10000,       /* Stack size of task */
    NULL,        /* parameter of the task */
    1,           /* priority of the task */
    &Task1,      /* Task handle to keep track of created task */
    0);          /* pin task to core 0 */


  //create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
  xTaskCreatePinnedToCore(
    Task2code,   /* Task function. */
    "Task2",     /* name of task. */
    10000,       /* Stack size of task */
    NULL,        /* parameter of the task */
    1,           /* priority of the task */
    &Task2,      /* Task handle to keep track of created task */
    1);          /* pin task to core 1 */

}
unsigned long start = 0;
unsigned long end = 0;

//Data read
void Task1code( void * pvParameters ) {
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());


  for (;;) {
    flashWrite();
    vTaskDelay(10);
  }
}

//CC1200 and Flash
void Task2code( void * pvParameters ) {
  Serial.print("Task2 running on core ");
  Serial.println(xPortGetCoreID());

  //timer sıfırla
  for (;;) {

    /*
      if(millis()-start>=20){

      readSensor();
      }*/

    vTaskDelay(1);

    if (count == 1) {
      // Comment out enter / exit to deactivate the critical section
      //start=millis();
      count = 0;
      readSensor();
      //end=millis();
      //Serial.printf("okuma suresi: %d \n",end-start);

    }
  }


}
void loop() {
  delay(1000000000000000000);
}

void sensorBegin() {
  /*
    myIMU.begin(74);
    myIMU.enableAccelerometer(20); //Send data update every 50ms
    myIMU.enableGyro(20); //Send data update every 50ms
    myIMU.enableMagnetometer(20); //Send data update every 50ms
    myIMU.enableRotationVector(20);
  */


  lis.begin_SPI(5, 18, 19, 23); // change this to 0x19 for alternative i2c address
  lis.setRange(H3LIS331_RANGE_100_G);   // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
  ms5611.reset();

  //GPS ayar
  Serial2.begin(9600, SERIAL_8N1, 14, 12);
  myGPS.begin(Serial2);
  myGPS.setSerialRate(38400);
  Serial.println("Trying 38400 baud");
  Serial2.begin(38400, SERIAL_8N1, 14, 12);
  myGPS.begin(Serial2);
  //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  myGPS.setNavigationFrequency(10); //Set output to 10 times a second
  //myGPS.setUART1Output(0); //Disable the UART1 port output
  //myGPS.setUART2Output(0); //Disable Set the UART2 port output
  //myGPS.setI2COutput(COM_TYPE_NMEA); //Set the I2C port to output UBX only (turn off NMEA noise)
  //myGPS.saveConfiguration();        //Save the current settings to flash and BBR

}
unsigned long start2 = 0;
unsigned long end2 = 0;
//sensor okuma fonksiyonu
void readSensor() {

  //readBno080();
  //vTaskDelay(1);

  //start2=millis();
  ms5611.tick();
  readMS5611();
  //end2=millis();
  //Serial.printf("takildi suresi MS5611: %d \n",end2-start2);

  //start2=millis();
  lis.getEvent(&event);
  readH3LIS331();
  //end2=millis();
  //Serial.printf("takildi suresi 400G: %d \n",end2-start2);
  readGPS();
  //start2=millis();
  writeData();
  //end2=millis();
  //Serial.printf("takildi suresi write: %d \n",end2-start2);

}


void readH3LIS331() {
  H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
  H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
}

void readBno080() {

  x_Accel = myIMU.getAccelX();
  y_Accel = myIMU.getAccelY();
  z_Accel = myIMU.getAccelZ();
  x_Gyro = myIMU.getGyroX();
  y_Gyro = myIMU.getGyroY();
  z_Gyro = myIMU.getGyroZ();
  x_Mag = myIMU.getMagX();
  y_Mag = myIMU.getMagY();
  z_Mag = myIMU.getMagZ();
  Roll = (myIMU.getRoll()) * 180.0 / PI;
  Pitch = (myIMU.getPitch()) * 180.0 / PI;
  Yaw = (myIMU.getYaw()) * 180.0 / PI;
}

void readMS5611() {
  Ms5611_data[0] = ms5611.get_temperature();
  Ms5611_data[1] = ms5611.get_pressure();
}

void readGPS() {

  if (countGps >= 4)
  {
    start2 = millis();
    countGps = 0;
    myGPS.checkUblox();
    if (nmea.isValid() == true)
    {
      GPS_data[0]  = nmea.getLatitude();
      GPS_data[1]  = nmea.getLongitude();
      nmea.getAltitude(GPS_data[2]);
      SIV = nmea.getNumSatellites();
      GPS_time[0] = nmea.getYear();
      GPS_time[1] = nmea.getMonth();
      GPS_time[2] = nmea.getDay();

      GPS_time[3] = nmea.getHour();
      GPS_time[4] = nmea.getMinute();
      GPS_time[5] = nmea.getSecond();
      end2 = millis();
      if (end2 - start2 > 1) {
        Serial.printf("takildi suresi GPS: %d \n", end2 - start2);
      }
      //Serial.printf("Lat: %ld Long: %ld (degrees * 10^-7) Alt: %ld (mm) SIV: %02X \n",GPS_data[0],GPS_data[1],GPS_data[2],(int)SIV);
      //Serial.printf("%d-%02d-%02d %02d:%02d:%02d\n",GPS_time[0],GPS_time[1],GPS_time[2],GPS_time[3],GPS_time[4],GPS_time[5]);
    }
    else {
      end2 = millis();
      if (end2 - start2 > 1) {
        Serial.printf("NOT Fix takildi suresi GPS: %d \n", end2 - start2);
      }
    }
  }

}

void writeData() {

  while (flashCheck1 && flashCheck2) {
    vTaskDelay(1);
  }

  if (dataCheck == 0) {
    All_data1[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data1[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data1[i] += String(x_Accel) + "," + String(y_Accel) + "," + String(z_Accel) + ",";
    All_data1[i] += String(x_Gyro) + "," + String(y_Gyro) + "," + String(z_Gyro) + ",";
    All_data1[i] += String(x_Mag) + "," + String(y_Mag) + "," + String(z_Mag) + ",";
    All_data1[i] += String(Roll) + "," + String(Pitch) + "," + String(Yaw) + ",";
    All_data1[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data1[i] += String(SIV) + ",";
    All_data1[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data1[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
    if (i == 9) {
      i = 0;
      flashCheck1 = 1;
      dataCheck = 1;
    }
    else {
      i++;
    }
  }
  else if (dataCheck == 1) {
    All_data2[i] = String(H3LIS331_data[0]) + "," + String(H3LIS331_data[1]) + "," + String(H3LIS331_data[2]) + ",";
    All_data2[i] += String(Ms5611_data[0]) + "," + String(Ms5611_data[1]) + ",";
    All_data2[i] += String(x_Accel) + "," + String(y_Accel) + "," + String(z_Accel) + ",";
    All_data2[i] += String(x_Gyro) + "," + String(y_Gyro) + "," + String(z_Gyro) + ",";
    All_data2[i] += String(x_Mag) + "," + String(y_Mag) + "," + String(z_Mag) + ",";
    All_data2[i] += String(Roll) + "," + String(Pitch) + "," + String(Yaw) + ",";
    All_data2[i] += String(GPS_data[0]) + "," + String(GPS_data[1]) + "," + String(GPS_data[2]) + ",";
    All_data2[i] += String(SIV) + ",";
    All_data2[i] += String(GPS_time[0]) + "-" + String(GPS_time[1]) + "-" + String(GPS_time[2]) + ",";
    All_data2[i] += String(GPS_time[3]) + "." + String(GPS_time[4]) + "." + String(GPS_time[5]) + "\n";
    if (i == 9) {
      i = 0;
      flashCheck2 = 1;
      dataCheck = 0;
    }
    else {
      i++;
    }
  }

}


void flashWrite() {

  if (flashCheck1 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash1[j] = All_data1[j];
    }
    flashCheck1 = 0;
    writeCheck1 = 1;
  }

  if (flashCheck2 == 1) {
    for (int j = 0; j < 9; j++) {
      All_data_flash2[j] = All_data2[j];
    }
    flashCheck2 = 0;
    writeCheck2 = 1;
  }

  if (writeCheck1 == 1) {
    //flashWriteData(All_data_flash1);
    writeCheck1 = 0;
  }

  if (writeCheck2 == 1) {
    //flashWriteData(All_data_flash2);
    writeCheck2 = 0;
  }

}

void flashWriteData(String a[10]) {
  if (hafiza == 0) {
    file = SPIFFS.open("/Data.txt", FILE_APPEND);
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else if (hafiza < 15) {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    hafiza++;
  }

  else {
    for (int j = 0; j < 9; j++) {
      file.print(a[j]);
    }
    file.close();
    hafiza = 0;
  }
}

void flashRead() {

  readFile(SPIFFS, "/Data.txt");
}

void flashClean() {

  deleteFile(SPIFFS, "/Data.txt");

}
void flashCreatPart() {
  writeFile(SPIFFS, "/Data.txt", All_data_flash1[0]);
}

void timerBegin() {
  ITimer1.attachInterruptInterval(20000, TimerHandler1);
}

void IRAM_ATTR TimerHandler1(void) {
  count = 1;
  countGps++;
}
void SFE_UBLOX_GPS::processNMEA(char incoming)
{
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void listDir(fs::FS &fs, const char * dirname, uint8_t levels) {
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if (!root) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root.isDirectory()) {
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while (file) {
    if (file.isDirectory()) {
      Serial.print("  DIR : ");
      Serial.print (file.name());
      time_t t = file.getLastWrite();
      struct tm * tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, ( tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour , tmstruct->tm_min, tmstruct->tm_sec);
      if (levels) {
        listDir(fs, file.name(), levels - 1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.print(file.size());
      time_t t = file.getLastWrite();
      struct tm * tmstruct = localtime(&t);
      Serial.printf("  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d\n", (tmstruct->tm_year) + 1900, ( tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour , tmstruct->tm_min, tmstruct->tm_sec);
    }
    file = root.openNextFile();
  }
}

void createDir(fs::FS &fs, const char * path) {
  Serial.printf("Creating Dir: %s\n", path);
  if (fs.mkdir(path)) {
    Serial.println("Dir created");
  } else {
    Serial.println("mkdir failed");
  }
}

void removeDir(fs::FS &fs, const char * path) {
  Serial.printf("Removing Dir: %s\n", path);
  if (fs.rmdir(path)) {
    Serial.println("Dir removed");
  } else {
    Serial.println("rmdir failed");
  }
}

void readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(fs::FS &fs, const char * path, String message) {
  File file = fs.open(path, FILE_WRITE);
  file.print(message);
  file.close();
}

void appendFile(fs::FS &fs, const char * path, String message) {
  File file = fs.open(path, FILE_APPEND);
  file.print(message);
  file.close();
}

void deleteFile(fs::FS &fs, const char * path) {
  fs.remove(path);
}

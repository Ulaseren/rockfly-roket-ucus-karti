
#include <Adafruit_H3LIS331.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO08x.h>
#include "FS.h"
#include "LITTLEFS.h"
#include <MicroNMEA.h>
#include "ms5611_driver.h"
#include "PCA9539.h"
#include <RadioLib.h>
#include "SparkFun_Ublox_Arduino_Library.h"
#include <SPI.h>
#include <Wire.h>

PCA9539 ioport(0x76);


char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
SFE_UBLOX_GPS myGPS;
int RXD2 = 26;
int TXD2 = 27;

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33);

float H3LIS331_data[3] = { 0, 0, 0 };
float Ms5611_data[2] = { 0, 0 };
float bnoAccel[3] = { 0, 0, 0 };
float bnoGyro[3] = { 0, 0, 0 };
float bnoMag[3] = { 0, 0, 0 };
float bnoRaw[3] = { 0, 0, 0 };
int bnoAccelC = 0, bnoGyroC = 0, bnoMagC = 0, bnoRawC = 0;
long GPS_data[3] = { 0, 0, 0 };
int GPS_time[6] = { 0, 0, 0, 0, 0, 0 };
uint32_t SIV;

uint8_t Led1 = pb3;
uint8_t Led2 = pb4;
uint8_t Pyro1 = pa4;
uint8_t Pyro2 = pa5;


#define battPin 35
float battMin = 0;
float battMax = 8;
float TempMin = 27.0;
float TempMax = 35.0;
#define PressMin 88800
#define PressMax 95500
#define gpsTime 2020
#define gpsLat 40.910752
#define gpsLong 29.217933
#define gpsAlt 0
float h3400gz = 2.0;
float h3400gxy = 1.0;


struct euler_t {
  float yaw;
  float pitch;
  float roll;
} ypr;

#define BNO08X_CS 5
#define BNO08X_INT 25
#define BNO08X_RESET 14
Adafruit_BNO08x bno08x(BNO08X_RESET);
sh2_SensorValue_t sensorValue;

uint8_t MS_CS = 2;
baro_ms5611 MS5611(MS_CS);

uint8_t accel200gCSPin = 17;
Adafruit_H3LIS331 lis = Adafruit_H3LIS331();

void setup() {
  Serial.begin(115200);
  while (!Serial)
    ;
  Serial.println("Serial    :[OK]");
  i2cScan();
  setPins();
  configLora();
  configGps();
  configBno();
  configH3lis();
  configMs5611();
  configIOex();
  readBat();
  //readBno();
  readH3lis();
  readMs5611();
  sendDataLora();
  readGPS();

  Serial.println(F("  _______        _      ____  _  __"));
  Serial.println(F(" |__   __|      | |    / __ \\| |/ /"));
  Serial.println(F("    | | ___  ___| |_  | |  | | ' / "));
  Serial.println(F("    | |/ _ \\/ __| __| | |  | |  <  "));
  Serial.println(F("    | |  __/\\__ \\ |_  | |__| | . \\ "));
  Serial.println(F("    |_|\\___||___/\\__|  \\____/|_|\\_\\"));
}

void loop() {
}

void i2cScan() {
  Serial.print("Wire Start   :");
  Serial.flush();
  Wire.begin();
  Serial.println("[OK]");
  Serial.flush();
  byte error, address;
  int nDevices = 0;
  byte PCA95 = 0;
  for (address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
    if (error == 0) {
      if (address == 118) {  //0x76
        Serial.println("PCA95 0x76    :[OK]");
        PCA95 = 1;
      } else {
        Serial.print("Bilinmeyen I2C aygit bulundu. Address 0x");
        if (address < 16)
          Serial.print("0");
        Serial.print(address, HEX);
        Serial.println("  !!!");
      }
      nDevices++;
    } else if (error == 4) {
      Serial.print("Bilinmeyen Hata. Address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.println(address, HEX);
    }
  }
  if (nDevices == 1 && PCA95 == 1) {
    Serial.println("Bulunan adres 1 :[OK]");
  } else {
    Serial.print("Bulunan adres :");
    Serial.println(nDevices);
    if (PCA95 == 0) Serial.println("BMP180 0x77    :[ERROR!]");
    Serial.println("I2C aygitlarini ve PULLUP direnclerini kontrol edin!!!");
    while (1)
      ;
  }
}
void setPins() {
  pinMode(BNO08X_CS, 1);
  pinMode(accel200gCSPin, 1);
  pinMode(MS_CS, 1);
  digitalWrite(BNO08X_CS, 1);
  digitalWrite(accel200gCSPin, 1);
  digitalWrite(MS_CS, 1);
}

void configLora() {
  Serial.print(F("[SX1262] Initializing ... "));  
  int state = radio.begin(868.0, 500.0, 7, 5, 0x34, 20);
  if (state == ERR_NONE) {
    Serial.println(F("lORA    :[OK]"));
  } else {
    Serial.print(F("lORA, HATA"));
    Serial.println(state);
    while (true)
      ;
  }
}


void configGps() {
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  myGPS.begin(Serial2);
  myGPS.setSerialRate(38400);
  Serial.println("Trying 38400 baud");
  Serial2.begin(38400, SERIAL_8N1, RXD2, TXD2);
  delay(100);
  if (myGPS.begin(Serial2) == false) {
    Serial.println(F("GPS Hatası"));
    while (1)
      ;
  }
  Serial.println(F("GPS    :[OK]"));
  //This will pipe all NMEA sentences to the serial port so we can see them
  myGPS.setNMEAOutputPort(Serial);
  myGPS.setNavigationFrequency(10);
}

void SFE_UBLOX_GPS::processNMEA(char incoming) {
  //Take the incoming char from the Ublox I2C port and pass it on to the MicroNMEA lib
  //for sentence cracking
  nmea.process(incoming);
}

void configBno() {
  if (!bno08x.begin_SPI(BNO08X_CS, BNO08X_INT)) {
    Serial.println("BNO08x   HATA");
    while (1) {
      delay(10);
    }
  }
  Serial.println("BNO08x   :[OK]");

  setReports();
  delay(100);
}

// Here is where you define the sensor outputs you want to receive
void setReports(void) {
  Serial.println("Setting desired reports");
  if (!bno08x.enableReport(SH2_ACCELEROMETER)) {
    Serial.println("Could not enable accelerometer");
  }
  if (!bno08x.enableReport(SH2_GYROSCOPE_CALIBRATED)) {
    Serial.println("Could not enable gyroscope");
  }
  if (!bno08x.enableReport(SH2_MAGNETIC_FIELD_CALIBRATED)) {
    Serial.println("Could not enable magnetic field calibrated");
  }
  if (!bno08x.enableReport(SH2_ROTATION_VECTOR)) {
    Serial.println("Could not enable rotation vector");
  }
}

void configH3lis() {
  if (!lis.begin_SPI(accel200gCSPin)) {
    Serial.println("H3lis   HATA");
    while (1) yield();
  }
  Serial.println("H3lis   :[OK]");
  lis.setRange(H3LIS331_RANGE_100_G);  // 100, 200, or 400 G!
  lis.setDataRate(LIS331_DATARATE_50_HZ);
}

void configMs5611() {
  MS5611.Initialize();
}

void configIOex() {
  Wire.begin(21, 22);
  ioport.pinMode(Pyro1, OUTPUT);
  ioport.pinMode(Pyro2, OUTPUT);
  ioport.digitalWrite(Pyro1, LOW);
  ioport.digitalWrite(Pyro2, LOW);
  delay(100);

  ioport.pinMode(Led1, OUTPUT);
  ioport.pinMode(Led2, OUTPUT);
  ioport.digitalWrite(Led1, LOW);
  ioport.digitalWrite(Led2, LOW);
  delay(100);
}
void readBat() {
  float R1 = 560000.0;  // 560K
  float R2 = 100000.0;  // 100K
  float value = 0.0f;
  value = analogRead(battPin);
  value += analogRead(battPin);
  value += analogRead(battPin);
  value = value / 3.0;
  value = (value * 3.33) / 4096.0;
  value = value / (R2 / (R1 + R2));
  Serial.println("[OK]");
  Serial.print("Batarya Voltaji  (");
  Serial.print(value);
  Serial.print(" V):");
  if (battMax > value > battMin) {
    Serial.println("[OK]");
  } else {
    Serial.println("Batarya Voltaji Uygun Aralikta Degil!!!");
    while (1)
      ;
  }
}
void quaternionToEuler(float qr, float qi, float qj, float qk, euler_t* ypr, bool degrees = false) {

  float sqr = sq(qr);
  float sqi = sq(qi);
  float sqj = sq(qj);
  float sqk = sq(qk);

  ypr->yaw = atan2(2.0 * (qi * qj + qk * qr), (sqi - sqj - sqk + sqr));
  ypr->pitch = asin(-2.0 * (qi * qk - qj * qr) / (sqi + sqj + sqk + sqr));
  ypr->roll = atan2(2.0 * (qj * qk + qi * qr), (-sqi - sqj + sqk + sqr));

  if (degrees) {
    ypr->yaw *= RAD_TO_DEG;
    ypr->pitch *= RAD_TO_DEG;
    ypr->roll *= RAD_TO_DEG;
  }
}
/*//Kart montajı tamamlandığında değerlere göre; 
//gyro için sallama testi 
//euler için döndürme testi yazılacak 
//magneto için değerlere göre mıknatıs yada döndürme testi yazılcakak   
  void readBno() {
    for(int i =0;i<40;i++){
   
    if (!bno08x.getSensorEvent(&sensorValue)) {
      return;
    }
    switch (sensorValue.sensorId) {
      case SH2_ACCELEROMETER:
        bnoAccel[0] += (sensorValue.un.accelerometer.x);
        bnoAccel[1] += (sensorValue.un.accelerometer.y);
        bnoAccel[2] += (sensorValue.un.accelerometer.z);
        bnoAccelC++;
        break;
      case SH2_GYROSCOPE_CALIBRATED:
        bnoGyro[0] += (sensorValue.un.gyroscope.x);
        bnoGyro[1] += (sensorValue.un.gyroscope.y);
        bnoGyro[2] += (sensorValue.un.gyroscope.z);
        bnoGyroC++;
        break;
      case SH2_MAGNETIC_FIELD_CALIBRATED:
        bnoMag[0] += (sensorValue.un.magneticField.x);
        bnoMag[1] += (sensorValue.un.magneticField.y);
        bnoMag[2] += (sensorValue.un.magneticField.z);
        bnoMagC++;
        break;
      case SH2_ROTATION_VECTOR:
        quaternionToEuler(sensorValue.un.rotationVector.real, sensorValue.un.rotationVector.i, sensorValue.un.rotationVector.j, sensorValue.un.rotationVector.k, &ypr, true);
        bnoRaw[0] += (ypr.yaw);
        bnoRaw[1] += (ypr.pitch);
        bnoRaw[2] += (ypr.roll);
        bnoRawC++;
        break;
    }
    delay(20);
    }
    bnoAccel[0]=bnoAccel[0]/bnoAccelC;
    bnoAccel[1]=bnoAccel[1]/bnoAccelC;
    bnoAccel[2]=bnoAccel[2]/bnoAccelC;

    bnoGyro[0]=bnoGyro[0]/bnoGyroC;
    bnoGyro[1]=bnoGyro[1]/bnoGyroC;
    bnoGyro[2]=bnoGyro[2]/bnoGyroC;

    bnoMag[0]=bnoMag[0]/bnoMagC;
    bnoMag[1]=bnoMag[1]/bnoMagC;
    bnoMag[2]=bnoMag[2]/bnoMagC;


  }
  */

void readH3lis() {
  for (int i = 0; i < 10; i++) {
    sensors_event_t event;
    lis.getEvent(&event);
    H3LIS331_data[0] = event.acceleration.x / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[1] = event.acceleration.y / SENSORS_GRAVITY_STANDARD;
    H3LIS331_data[2] = event.acceleration.z / SENSORS_GRAVITY_STANDARD;
    delay(100);
  }
  if (h3400gxy - 0, 5 < (H3LIS331_data[0] / 10) < h3400gxy + 0, 5 && h3400gxy - 0, 5 < (H3LIS331_data[1] / 10) < h3400gxy + 0, 5 && h3400gz - 1 < (H3LIS331_data[2] / 10) < h3400gz + 1) {
    Serial.println("H3LIS331 VERİ TESTİ   [OK]");
  } else {
    Serial.println("H3LIS331 Bozuk data");
    while (1)
      ;
  }
}

void readMs5611() {
  for (int i = 0; i < 10; i++) {
    MS5611.updateTemperature();
    MS5611.getPressure();
    Ms5611_data[0] += MS5611.getTemperature_degC();
    Ms5611_data[1] += MS5611.getPressure_mbar();
    delay(100);
  }
  if (TempMin < (Ms5611_data[0] / 10) < TempMax && PressMin < (Ms5611_data[1] / 10) < PressMax) {
    Serial.println("MS5611 VERİ TESTİ   [OK]");
  } else {
    Serial.println("MS5611 Bozuk data");
    while (1)
      ;
  }
}

void sendDataLora() {
  Serial.println(F("LORA TEST "));
  int state = radio.transmit("Hello World!");
  if (state == ERR_NONE) {
    // the packet was successfully transmitted
    Serial.println(F("    [OK] "));
    // print measured data rate
    //Serial.print(F("[SX1262] Datarate:\t"));
    //Serial.print(radio.getDataRate());
    //Serial.println(F(" bps"));

  } else if (state == ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    Serial.println(F("too long!"));
    while (1)
      ;
  } else if (state == ERR_TX_TIMEOUT) {
    // timeout occured while transmitting packet
    Serial.println(F("timeout!"));
    while (1)
      ;
  } else {
    // some other error occurred
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (1)
      ;
  }
}

void readGPS() {
  Serial.println(F("GPS TEST "));
  while (1) {
    delay(1000);
    Serial.print(F("."));
    myGPS.checkUblox();
    if (nmea.isValid()) {
      if (nmea.getYear() > gpsTime) {
        if (nmea.getLatitude() > gpsLat - 1 && nmea.getLatitude() < gpsLat + 1 && nmea.getLongitude() > gpsLong - 1 && nmea.getLongitude() < gpsLong + 1) {
        Serial.println(F("[OK]"));
        break;
      }
      }
    }
  }
  
}
    


